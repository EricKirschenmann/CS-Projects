﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab11
{
    class Program
    {
        static void Main(string[] args)
        {
            // get a string from the user and pass it through the methods
            Console.Write("Please enter a string: ");
            PrintReverse(StringToCharArray(Console.ReadLine()));
        }

        static char[] StringToCharArray(string input)
        {
            // initialize array to the length of the string
            char[] chars = new char[input.Length];

            // copy each element into the array while looping through the string
            for (int x = 0; x < chars.Length; x++)
            {
                chars[x] = input[x];
            }

            return chars;
        }

        static void PrintReverse(char[] chars)
        {
            // loop through the array in reverse
            for (int x = (chars.Length - 1); x >= 0; x--)
            {
                // print the current char
                Console.Write(chars[x]);
            }
            Console.WriteLine();
        }
    }
}
