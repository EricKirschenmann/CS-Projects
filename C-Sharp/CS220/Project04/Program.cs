﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project04
{
    class Program
    {
        static void Main(string[] args)
        {
            // get the initial loan amount
            Console.Write("Please enter the initial cost: ");
            double loan = double.Parse(Console.ReadLine());

            // get interest rate (expects 18 vs .18)
            Console.Write("Please enter the annual interest rate: ");
            double rate = double.Parse(Console.ReadLine()) / 100.0;

            // get monthly payment
            Console.Write("Please enter the monthly payment: ");
            double monthly = double.Parse(Console.ReadLine());

            // print loan info
            Console.WriteLine();
            Console.WriteLine($"Loan Amount: {loan:C2}\tAnnual Interest Rate: {rate:P1}\tMonthly Payment: {monthly:C2}");

            // print headers
            Console.WriteLine();
            Console.WriteLine("Month#\tTotal Payment\tAmount Interest Paid\tAmount Debt Paid\tLoan Balance");
            Console.WriteLine("------\t-------------\t--------------------\t----------------\t------------");

            // initialize variables
            double interest, debt, totalInterest = 0.0;
            int month = 1;

            // make payments month by month until loan is gone
            while (loan > 0.0)
            {
                // calculate interest
                interest = loan * (rate / 12.0);
                totalInterest += interest;

                // amount paid will be whichever is lower
                debt = Math.Min(loan, (monthly - interest));

                // make payment
                loan -= debt;

                // print this month's data
                Console.WriteLine($"{month}\t{(debt + interest):C2}\t\t{interest:C2}\t\t\t{debt:C2}\t\t\t{loan:C2}");

                // next month
                month++;
            }

            // display total paid in interest
            Console.WriteLine();
            Console.WriteLine($"Total Amount of Interest Paid: {totalInterest:C2}");
        }
    }
}
