﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculateBmi();
            CompareStringLength();
        }

        static void CalculateBmi()
        {
            // get the user's height
            Console.Write("Please enter your height in inches: ");
            double height = double.Parse(Console.ReadLine());

            // get the user's weight
            Console.Write("Please enter your weight in pounds: ");
            double weight = double.Parse(Console.ReadLine());

            // calculate the user's BMI
            double bmi = (weight * 703.0) / (height * height);

            // display BMI
            Console.WriteLine($"You BMI is {bmi:F2}");
        }

        static void CompareStringLength()
        {
            // get two words from the user
            Console.WriteLine("Enter two words separated by a space on the same line: ");
            string words = Console.ReadLine();

            // get the separate words
            string word1 = words.Split(' ')[0];
            string word2 = words.Split(' ')[1];

            // calculate the shorter of the two lengths
            int shortest = Math.Min(word1.Length, word2.Length);

            Console.WriteLine($"The shorter word is {shortest} characters long.");
        }
    }
}
