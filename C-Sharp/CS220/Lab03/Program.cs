﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03
{
    class Program
    {
        // constants containing the value of the different coins
        private const double PennyValue = 0.01;
        private const double NickelValue = 0.05;
        private const double DimeValue = 0.10;
        private const double QuarterValue = 0.25;

        static void Main(string[] args)
        {
            PrintPractice();
            CalculateCoinValue();
        }

        static void PrintPractice()
        {
            // print using only one print statement
            Console.WriteLine("*********************************************************************\n" +
                "* Wisdom is the right use of knowledge. To know is not to be wise.  *\n" +
                "* Many men know a great deal, and are all the greater fools for it. *\n" +
                "* There is no fool so great a fool as a knowing fool.               *\n" +
                "* But to know how to use knowledge is to have wisdom.               *\n" +
                "*                                                 ~Charles Spurgeon *\n" +
                "*********************************************************************");
        }

        static void CalculateCoinValue()
        {
            // get number of pennies
            Console.Write("How many pennies do you have: ");
            int pennies = int.Parse(Console.ReadLine());

            // get number of nickels
            Console.Write("How many nickels do you have: ");
            int nickels = int.Parse(Console.ReadLine());

            // get number of dimes
            Console.Write("How many dimes do you have: ");
            int dimes = int.Parse(Console.ReadLine());

            // get number of quarters
            Console.Write("How many quarters do you have: ");
            int quarters = int.Parse(Console.ReadLine());

            // calculate the value of all the coins
            double total = (pennies * PennyValue) + (nickels * NickelValue) + (dimes * DimeValue) + (quarters * QuarterValue);

            // display total value
            Console.WriteLine($"You have exactly {total:C2}");
        }
    }
}
