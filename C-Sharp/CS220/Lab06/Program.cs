﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab06
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckTld();
            CheckWeightClass();
        }

        static void CheckTld()
        {
            // get a website's address from the user
            Console.Write("Please enter a web address: ");
            string address = Console.ReadLine();

            // get the TLD from the web address
            string tld = address.Substring(address.LastIndexOf("."));

            // will contain the type of address
            string result = "The address is ";

            // check and append the type of TLD onto the string
            switch (tld)
            {
                case ".gov":
                    result += "a government address.";
                    break;
                case ".edu":
                    result += "a university address.";
                    break;
                case ".com":
                    result += "a business address.";
                    break;
                case ".org":
                    result += "an organization addresss.";
                    break;
                default:
                    result += "from an unknown entity.";
                    break;
            }

            // display the type of TLD
            Console.WriteLine(result);
        }

        static void CheckWeightClass()
        {
            // get the user's sex as either m or f
            Console.Write("Please enter your sex (m or f): ");
            char sex = Console.ReadLine().ToLower().ToCharArray()[0];

            // get the user's weight in pounds
            Console.WriteLine("Please enter your weight in pounds: ");
            double weight = double.Parse(Console.ReadLine());

            // check if male or female
            if (sex == 'm')
            {
                // check weight and place the user into a weight class
                if (weight >= 0.0 && weight < 170.0)
                {
                    Console.WriteLine("You've been placed into the men's lightweight class.");
                }
                else if (weight >= 170.0 && weight < 220.0)
                {
                    Console.WriteLine("You've been placed into the men's middleweight class.");
                }
                else if (weight >= 220.0)
                {
                    Console.WriteLine("You've been placed into the men's heavyweight class.");
                }
                else
                {
                    // out of bounds weight
                    Console.WriteLine("Sorry, couldn't place you into a weight class.");
                }
            }
            else if (sex == 'f')
            {
                if (weight >= 0.0 && weight < 130.0)
                {
                    Console.WriteLine("You've been placed into the women's lightweight class.");
                }
                else if (weight >= 130.0 && weight < 185.0)
                {
                    Console.WriteLine("You've been placed into the women's middleweight class.");
                }
                else if (weight >= 185.0)
                {
                    Console.WriteLine("You've been placed into the women's heavyweight class.");
                }
                else
                {
                    // out of bounds weight
                    Console.WriteLine("Sorry, couldn't place you into a weight class.");
                }
            }
            else
            {
                // couldn't find specified sex
                Console.WriteLine("Sorry, couldn't place you into a weight class.");
            }
        }
    }
}
