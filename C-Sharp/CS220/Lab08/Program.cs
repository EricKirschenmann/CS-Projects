﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab08
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculateSummation();
            CalculateFactorial();
        }

        static int GetPositiveInteger()
        {
            int output = -1;

            // looking for a number zero or above
            while (output < 0)
            {
                // get a number from the user
                Console.Write("Enter a positive integer: ");

                // attempt to parse an integer out of the user's input
                if (!int.TryParse(Console.ReadLine(), out output))
                {
                    // if it failed to convert, usually text input so invalid
                    output = -1;
                }
            }

            // return the number
            return output;
        }

        static void CalculateSummation()
        {
            // get a starting number
            int start = GetPositiveInteger();

            int sum = 0;

            // calculate the summation from start to start + 10
            for (int x = start; x <= (start + 10); x++)
            {
                sum += x;
            }

            // display the calculated summation
            Console.WriteLine($"The summation from {start} to {start + 10} is {sum}.");
        }

        static void CalculateFactorial()
        {
            // get a starting number
            int start = GetPositiveInteger();

            int factorial = 1;

            // calculate the factorial
            for (int x = start; x > 0; x--)
            {
                factorial *= x;
            }

            // display the calculated factorial
            Console.WriteLine($"The factorial {start}! is {factorial}.");
        }
    }
}
