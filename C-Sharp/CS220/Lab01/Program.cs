﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab01
{
    class Program
    {
        static void Main(string[] args)
        {
            Variables();
            ArithmeticOperators();
        }

        static void Variables()
        {
            // this example shows how to declare and initialize variables
            int testGrade = 100;
            long cityPopulation = 425612340L;
            byte ageInYears = 19;

            float salesTax = 0.05F;
            double interestRate = 0.725;
            double avogadroNumber = +6.022E23;
            // avogadroNumber is represeented in scientific notation
            // its value is 6.022 x 10^23

            char finalGrade = 'A';
            bool isEmpty = true;

            Console.WriteLine($"testGrade is {testGrade}");
            Console.WriteLine($"cityPopulation is {cityPopulation}");
            Console.WriteLine($"ageInYears is {ageInYears}");
            Console.WriteLine($"salesTax is {salesTax}");
            Console.WriteLine($"interestRate is {interestRate}");
            Console.WriteLine($"avogadroNumber is {avogadroNumber}");
            Console.WriteLine($"finalGrade is {finalGrade}");
            Console.WriteLine($"isEmpty is {isEmpty}");
        }

        static void ArithmeticOperators()
        {
            // calculate the cost of lunch
            double salad = 5.95;
            double water = 0.89;
            Console.WriteLine($"The cost of lunch is {(salad + water):C2}");

            // calculate your age as of a certain year
            int targetYear = 2011;
            int birthYear = 1993;
            Console.WriteLine($"Your age in {targetYear} is {(targetYear - birthYear)}");

            // calculate the total calories of apples
            int caloriesPerApple = 127;
            int numberOfApples = 3;
            Console.WriteLine($"The calories in {numberOfApples} apples is {(caloriesPerApple * numberOfApples)}");

            // calculate miles per gallon
            double miles = 426.8;
            double gallons = 15.2;
            double mileage = miles / gallons;
            Console.WriteLine($"The mileage is {mileage} miles per gallon.");
        }
    }
}
