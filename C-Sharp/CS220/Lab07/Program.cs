﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab07
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintPractice();
            CalculateSquareRoots();
        }

        static void PrintPractice()
        {
            // get a number from the user
            Console.Write("How many times to print? ");
            int count = int.Parse(Console.ReadLine());

            // print while looping as many times as inputted
            while (count > 0)
            {
                Console.WriteLine("Hello World");
                count--;
            }
        }

        static void CalculateSquareRoots()
        {
            // will contain the number to take the square root of
            double start = 0.0;

            // get a starting number larger than 10
            while (start <= 10.0)
            {
                Console.Write("Please enter a number greater than 10: ");
                start = double.Parse(Console.ReadLine());
            }

            // will contain the square root
            double root;
            // will contain the number of operations performed (times looped)
            int count = 0;

            while (start >= 1.01)
            {
                // calculate square root
                root = Math.Sqrt(start);
                // display square root
                Console.WriteLine($"The square root of {start:F3} is {root:F3}");
                // going to take the square root of the next number
                start = root;
                // count how many loops performed
                count++;
            }

            // display the total number of times printed
            Console.WriteLine($"You performed {count} square root operations.");
        }
    }
}
