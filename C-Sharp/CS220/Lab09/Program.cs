﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab09
{
    class Program
    {
        static void Main(string[] args)
        {
            // test default constructor and ToString()
            House defaultHouse = new House();
            Console.WriteLine(defaultHouse.ToString());

            Console.WriteLine();

            // test overloaded constructor and ToString()
            House bigHouse = new House(1400000, "Pasadena", 6);
            Console.WriteLine(bigHouse.ToString());

            // display property values before modifying
            Console.WriteLine("Before modifying big house using setters: ");
            Console.WriteLine("Big house value: " + bigHouse.Value.ToString("C0"));
            Console.WriteLine("Big house location: " + bigHouse.Location);
            Console.WriteLine("Big house bedrooms: " + bigHouse.Bedrooms);

            // set properties to new values
            bigHouse.Value = 3200000;
            bigHouse.Location = "Hollywood";
            bigHouse.Bedrooms = 14;

            Console.WriteLine();

            // display property values after modifying
            Console.WriteLine("After modifying big house using setters: ");
            Console.WriteLine("Big house value: " + bigHouse.Value.ToString("C0"));
            Console.WriteLine("Big house location: " + bigHouse.Location);
            Console.WriteLine("Big house bedrooms: " + bigHouse.Bedrooms);

            Console.WriteLine();

            // check Equals() with inequivalent objects
            Console.Write("The default house and big house ");
            Console.WriteLine((bigHouse.Equals(defaultHouse)) ? "are the SAME houses!" : "are DIFFERENT houses!");

            Console.WriteLine();

            // check Equals() with equivalent objects
            House identical = new House(3200000, "Hollywood", 14);
            Console.Write("The identical big house and big house ");
            Console.WriteLine((bigHouse.Equals(identical)) ? "are the SAME houses!" : "are DIFFERENT houses!");
        }
    }
}
