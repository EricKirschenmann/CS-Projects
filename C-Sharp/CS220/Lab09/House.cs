﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab09
{
    class House : IEquatable<House>
    {
        private double _value;
        private string _location;
        private int _bedrooms;

        public int Bedrooms { get => _bedrooms; set => _bedrooms = value; }
        public string Location { get => _location; set => _location = value; }
        public double Value { get => _value; set => _value = value; }

        public House()
        {
            Value = 100000;
            Location = "Azusa";
            Bedrooms = 2;
        }

        public House(double value, string location, int bedrooms)
        {
            Value = value;
            Location = location;
            Bedrooms = bedrooms;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as House);
        }

        public bool Equals(House other)
        {
            return other != null &&
                   Bedrooms == other.Bedrooms &&
                   Location == other.Location &&
                   Value == other.Value;
        }

        public override int GetHashCode()
        {
            var hashCode = 1883274828;
            hashCode = hashCode * -1521134295 + Bedrooms.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Location);
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(House house1, House house2)
        {
            return EqualityComparer<House>.Default.Equals(house1, house2);
        }

        public static bool operator !=(House house1, House house2)
        {
            return !(house1 == house2);
        }

        public override string ToString()
        {
            return "This house is in " +
                Location +
                ", is worth " +
                Value.ToString("C2") +
                " and has " +
                Bedrooms +
                " bedrooms.";
        }
    }
}
