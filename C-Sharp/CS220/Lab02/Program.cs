﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab02
{
    class Program
    {
        static void Main(string[] args)
        {
            ConvertTemperature();
        }

        static void ConvertTemperature()
        {
            // get a starting temperature from the user
            Console.Write("Please enter a temperature in Fahrenheit: ");
            double tempF = double.Parse(Console.ReadLine());

            // convert to Celsius
            double tempC = (5.0 / 9.0) * (tempF - 32.0);

            // display original and converted temperatures
            Console.WriteLine();
            Console.WriteLine($"The original temperature (in Fahrenheit) is {tempF} degrees.");
            Console.WriteLine($"The Celsius equivalent is {tempC:F0} degrees.");

            // convert from Celsius to Fahrenheit
            tempF = ((9.0 / 5.0) * tempC) + 32.0;

            // display converted temperature
            Console.WriteLine($"Converted back to Fahrenheit: {tempF} degrees.");
        }
    }
}
