﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project05
{
    class Car
    {
        // instance variables
        private string _carModel;
        private double _milesPerGallon;
        private double _gasTankSize;
        private double _gallonsInTank;

        public string CarModel { get => _carModel; set => _carModel = value; }
        public double MilesPerGallon { get => _milesPerGallon; set => _milesPerGallon = value; }
        public double GasTankSize { get => _gasTankSize; set => _gasTankSize = value; }
        public double GallonsInTank { get => _gallonsInTank; set => _gallonsInTank = Math.Min(value, _gasTankSize); }

        public Car()
        {
            CarModel = "Honda Accord";
            MilesPerGallon = 22.0;
            GasTankSize = 15.0;
            GallonsInTank = 15.0;
        }

        public Car(string carModel, double milesPerGallon, double gasTankSize, double gallonsInTank)
        {
            CarModel = carModel;
            MilesPerGallon = milesPerGallon;
            GasTankSize = gasTankSize;
            GallonsInTank = gallonsInTank;
        }

        public bool DriveDistance(double miles)
        {
            // calculate how many gallons it would take to go the given distance
            double burnt = miles / MilesPerGallon;

            // check if the car can travel the given distance
            if (burnt <= GallonsInTank)
            {
                // remove the used gas
                GallonsInTank -= burnt;

                // display how many gallons were burnt and how far the car can still travel
                Console.WriteLine($"{burnt:F2} gallons burnt. Can travel {(GallonsInTank * MilesPerGallon):F2} more on this tank.");

                // successfully traveled
                return true;
            }
            else
            {
                // can't travel given distance, display how far the car made with it's current gas
                Console.WriteLine($"{(MilesPerGallon * GallonsInTank):F2} were traveled before running out of fuel.");

                // remove the used gas
                GallonsInTank = 0.0;

                // failed to travel the given distance
                return false;
            }
        }

        public double FillTank()
        {
            // find the amount to fill and fill
            double fill = GasTankSize - GallonsInTank;
            GallonsInTank = GasTankSize;
            
            // display how much filled and how far can go now
            Console.WriteLine($"{fill:F2} gallons were added to your tank.");
            Console.WriteLine($"Can travel {(MilesPerGallon * GallonsInTank):F2} more miles on this tank.");

            return fill;
        }

        public Car ConvertToHybrid()
        {
            return new Car(CarModel + " Hybrid", MilesPerGallon + 10, GasTankSize, GallonsInTank);
        }

        public override string ToString()
        {
            return $"The {CarModel} gets {MilesPerGallon:F1} mpg and has {GallonsInTank:F1} gallons of gas in its {GasTankSize:F1} gallon tank.";
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            else if (ReferenceEquals(this, obj))
            {
                // check if obj is just a reference to this car
                return true;
            }
            else if (obj is Car car)
            {
                // check instance variables
                return car.CarModel.Equals(CarModel) 
                    && car.MilesPerGallon == MilesPerGallon 
                    && car.GallonsInTank == GallonsInTank 
                    && car.GasTankSize == GasTankSize;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            var hashCode = 1569744816;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CarModel);
            hashCode = hashCode * -1521134295 + MilesPerGallon.GetHashCode();
            hashCode = hashCode * -1521134295 + GasTankSize.GetHashCode();
            hashCode = hashCode * -1521134295 + GallonsInTank.GetHashCode();
            return hashCode;
        }
    }
}
