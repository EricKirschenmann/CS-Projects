﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Project05
{
    class Program
    {
        // CHECKS
        // [Check 1] carModel getter/setter
        // [Check 2] milesPerGallongetter/setter
        // [Check 3] gasTankSize getter/setter
        // [Check 4] gallonsInTank getter/setter
        // [Check 5] driveDistance() good drive output
        // [Check 6] driveDistance() true return
        // [Check 7] driveDistance() bad drive output
        // [Check 8] driveDistance() false return
        // [Check 9] fillTank() normal fill return
        // [Check 10] fillTank() overfill output
        // [Check 11] fillTank() overfill return
        // [Check 12] toString() output
        // [Check 13] convertToHybrid() return & toString() print
        // [Check 14] equals() false return
        // [Check 15] equals() true return

        static void Main(string[] args)
        {
            // car initializers
            Car generic = new Car();
            Car viper = new Car("Dodge Viper", 18, 20, 15);
            Car viperHybrid; // Will use later
            Car drivenViper = new Car("Dodge Viper GTS", 16, 22, 22); // Will use later

            //testing getters/setters
            viper.CarModel = "Dodge Viper GTS";
            viper.MilesPerGallon = 16;
            viper.GasTankSize = 22;
            viper.GallonsInTank = 25;
            if (!viper.CarModel.Equals("Dodge Viper GTS"))
                AGP("-2pts for model getter/setter mismatch", 1); // Check #1
            if (!DoubleEquals(viper.MilesPerGallon, 16))
                AGP("-2pts for mpg getter/setter mismatch", 2); // Check #2
            if (!DoubleEquals(viper.GasTankSize, 22))
                AGP("-2pts for gasTankSize getter/setter mismatch", 3); // Check #3
            if (!DoubleEquals(viper.GallonsInTank, 22)) // Should realize tried to overfill
                AGP("-2pts for gallonsInTank getter/setter mismatch", 4); // Check #4

            // test driveDistance() function (8 pts total)
            AGMCP("Should print 12.5 gal burnt, 152 more miles on tank (-2pts if not)", 5);  // Check #5
            if (!viper.DriveDistance(200))
                AGP("-2pts for driveDistance improper return (should be true)", 6); // Check #6
            EMC();
            AGMCP("Should print that 152 miles travelled before running out of gas (-2pts if not)", 7);  // Check #7
            if (viper.DriveDistance(200))
                AGP("-2pts for driveDistance improper return (should be false)", 8);  // Check #8
            EMC();

            // test fillTank() function (6pts)
            if (!DoubleEquals(viper.FillTank(), 22))
                AGP("-2pts for fillTank improper return (should be 22)", 9); // Check #9
            viper.DriveDistance(100); // burn some fuel to test half-fill
            AGMCP("Should print that 6.25 gallons were added to your tank and can travel 352 more miles (-2pts if not)", 10); // Check #10
            if (!DoubleEquals(viper.FillTank(), 6.25))
                AGP("-2pts for fillTank improper return (should be 6.25)", 11); // Check #11
            EMC();

            // test toString() function (2pts)
            AGMCP("Should print out some form of \"Dodge Viper GTS, 16mpg, 22 gallons gas, & 22 gallon tank\" (-2pts)", 12); // Check #12
            Console.WriteLine(viper.ToString());
            EMC();

            // test convertToHybrid() function (2pts)
            viperHybrid = viper.ConvertToHybrid();
            if (!((viperHybrid.CarModel.Equals(viper.CarModel + " Hybrid", StringComparison.InvariantCultureIgnoreCase) || viperHybrid.CarModel.Equals(viper.CarModel + "Hybrid", StringComparison.InvariantCultureIgnoreCase))
                    && DoubleEquals(viper.MilesPerGallon + 10, viperHybrid.MilesPerGallon)))
            {
                AGP("-2pts for convertToHybrid hybrid conversion (should be Dodge Viper GTS Hybrid @ 26mpg)", 13);  // Check #13
                Console.WriteLine("Converted to: " + viperHybrid.ToString());
            }

            // test equals() function (4pts)
            //AGMCP("Grader: Please rename their Equals function to equals, if necessary, for no penalty.", -1);
            //EMC();
            if (viper.Equals(viperHybrid))
                AGP("-2pts for equals check (should be false)", 14); // Check #14
            if (!viper.Equals(drivenViper))
                AGP("-2pts for equals check (should be true)", 15); // Check #15
        }

        // Autograder helper functions: AGP = "Auto-Grader Print";
        static void AGP(String s, int checkNum)
        {
            try { Thread.Sleep(300); } catch (ThreadInterruptedException e) { Console.WriteLine(e.Message); }
            if (checkNum >= 0)
                Console.Error.WriteLine("** [Check " + checkNum + "] AG: " + s);
            else
                Console.Error.WriteLine("** [INFO] AG: " + s);
        }

        // AGMCP = "Auto-Grader Manual Check Print"
        static void AGMCP(String s, int checkNum)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            try { Thread.Sleep(300); } catch (ThreadInterruptedException e) { Console.WriteLine(e.Message); }
            if (checkNum >= 0)
                Console.Error.WriteLine("\n**** [Check " + checkNum + "] BEGIN MANUAL-CHECK: " + s);
            else
                Console.Error.WriteLine("\n**** [INFO] BEGIN MANUAL-CHECK: " + s);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        // EMC = "End Manual Check"
        static void EMC()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            try { Thread.Sleep(300); } catch (ThreadInterruptedException e) { Console.WriteLine(e.Message); }
            Console.Error.WriteLine("------------------------------END MANUAL CHECK------------------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        static bool DoubleEquals(double d1, double d2)
        {
            double thresh = .1;
            return (Math.Abs(d1 - d2) < thresh);
        }
    }
}
