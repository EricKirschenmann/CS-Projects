﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project01
{
    class Program
    {
        static void Main(string[] args)
        {
            // get annual interest rate
            Console.Write("What is your annual interest rate as a decimal? (ex: 0.045): ");
            double rate = double.Parse(Console.ReadLine());

            // get the number of years
            Console.Write("How many years will your mortgage be held? ");
            int years = int.Parse(Console.ReadLine());

            // get starting mortgage
            Console.Write("What amount of the mortgage did you borrow? ");
            double mortgage = double.Parse(Console.ReadLine());

            // calculate monthly payment
            double monthly = ((rate / 12.0) * mortgage) / (1 - (1 / (Math.Pow((1 + (rate / 12.0)), (12.0 * years)))));

            // calculate total payment over the years
            double total = monthly * 12.0 * years;

            // display mortgage info
            Console.WriteLine();
            Console.WriteLine($"The number {rate} can be represented as {rate:P1}");
            Console.WriteLine($"The mortgage amount is {mortgage:C2}");
            Console.WriteLine($"The monthly payment is {monthly:C2}");
            Console.WriteLine($"The total payment over the years is {total:C2}");
            Console.WriteLine($"The overpayment is {(total - mortgage):C2}");
            Console.WriteLine($"The overpayment as a percentage of the mortgage is {((total - mortgage) / mortgage):P1}");
        }
    }
}
