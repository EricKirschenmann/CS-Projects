﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project03
{
    class Program
    {
        static void Main(string[] args)
        {
            // get an equation seperated by spaces
            Console.Write("Please enter an equation: ");
            string input = Console.ReadLine();

            // break input into pieces
            double num1 = double.Parse(input.Split(' ')[0]);
            char op = input.Split(' ')[1][0];
            double num2 = double.Parse(input.Split(' ')[2]);

            // create the base equation
            string equation = $"{num1:F2} {op} {num2:F2} = ";

            // check which operator and perform the equation
            switch (op)
            {
                case '+':
                    equation += $"{(num1 + num2):F2}";
                    break;
                case '-':
                    equation += $"{(num1 - num2):F2}";
                    break;
                case '*':
                    equation += $"{(num1 * num2):F2}";
                    break;
                case '/':
                    // check to make sure not dividing by zero
                    if (num2 != 0.0)
                    {
                        equation += $"{(num1 / num2):F2}";
                    }
                    else
                    {
                        equation = "Sorry, cannot divide by zero.";
                    }
                    break;
                case '^':
                    equation += $"{(Math.Pow(num1,num2)):F2}";
                    break;
                case '%':
                    // check to make sure not dividing by zero
                    if (num2 != 0.0)
                    {
                        equation += $"{(num1 % num2):F2}";
                    }
                    else
                    {
                        equation = "Sorry, cannot divide by zero.";
                    }
                    break;
                default:
                    equation = $"Sorry, {op} not supported.";
                    break;
            }

            // display results
            Console.WriteLine(equation);
        }
    }
}
