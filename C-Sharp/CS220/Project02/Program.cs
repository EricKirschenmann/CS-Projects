﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project02
{
    class Program
    {
        static void Main(string[] args)
        {
            // will be split up into parts using SubString()
            string template = "Hello, %NAME%, how are you doing this fine %DAY%? I hope great!";

            // break into parts using SubString()
            string part1 = template.Substring(0, template.IndexOf("%NAME%"));
            string part2 = template.Substring(template.IndexOf(", how"), (template.IndexOf("%DAY%") - template.IndexOf(", how")));
            string part3 = template.Substring(template.IndexOf("?"));

            // get name from user
            Console.Write("What is your name? ");
            string name = Console.ReadLine();

            // get day from user
            Console.Write("What day is it today? ");
            string day = Console.ReadLine();

            // construct full greeting
            string greeting = part1 + name.ToUpper() + part2 + day.ToLower() + part3;

            // display string info to user
            Console.WriteLine(greeting);
            Console.WriteLine($"\t{name} is {name.Length} characters long.");
            Console.WriteLine($"\t{day} is {day.Length} characters long.");
            Console.WriteLine($"\tThe longest input string is {Math.Max(name.Length, day.Length)} characters long.");
            Console.WriteLine($"\tThe shortest input string is {Math.Min(name.Length, day.Length)} characters long.");
            Console.WriteLine($"\tThe greeting sentance, with the input name and day, is {greeting.Length} characters long.");
        }
    }
}
