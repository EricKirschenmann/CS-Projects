﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace Lab05
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckWeather();
            CheckLogin();
        }

        static void CheckWeather()
        {
            // get temperature from the user
            double tempC = double.Parse(Interaction.InputBox("What is the temperature in Celsius?", "Temperature"));

            // convert to fahrenheit
            double tempF = ((9.0 / 5.0) * tempC) + 32.0;

            // display what to wear based on converted temperature
            if (tempF >= 70.0)
            {
                Interaction.MsgBox("It should be okay to wear shorts today.", MsgBoxStyle.OkOnly, "Temperature");
            }
            else
            {
                Interaction.MsgBox("You might think about wearing a jacket.", MsgBoxStyle.OkOnly, "Temperature");
            }
        }

        static void CheckLogin()
        {
            // get username and passwords from the user
            string username = Interaction.InputBox("Please enter your username:");
            string password1 = Interaction.InputBox("Please re-enter your password:");
            string password2 = Interaction.InputBox("Please enter your password:");

            string result = "";

            // check if the log in rules are met with the user's credentials
            if (username.Length >= 6 && username.Length <= 10)
            {
                if (!username.Contains('&'))
                {
                    if (password1 == password2)
                    {
                        result = "Welcome, " + username + ".";
                    }
                    else
                    {
                        result = "Passwords must match.";
                    }
                }
                else
                {
                    result = "Sorry, username cannot contain '&'.";
                }
            }
            else if (username.Length < 6)
            {
                result = "Sorry, username must contain more than 6 characters.";
            }
            else if (username.Length > 10)
            {
                result = "Sorry, username must contain less than 10 characters.";
            }
            else
            {
                result = "Sorry, could not log on.";
            }

            // display the result of the log in check
            Interaction.MsgBox(result);
        }
    }
}
