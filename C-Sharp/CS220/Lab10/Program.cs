﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab10
{
    enum Coin
    {
        Penny,
        Nickel,
        Dime,
        Quarter,
        Unknown
    }

    enum Shape
    {
        Triangle,
        Square,
        Rectangle
    }

    class Program
    {
        static void Main(string[] args)
        {
            CheckCoin();
            Area.PrintArea(Shape.Rectangle, 3.2, 2.5);
            Area.PrintArea(Shape.Square, 2.4, 2.4);
            Area.PrintArea(Shape.Triangle, 3.5, 2.2);

            // invalid shape
            Area.PrintArea((Shape)5, 2.2, 1.2);
        }

        static void CheckCoin()
        {
            // get the first letter of a coin from the user
            Console.Write("Please enter a type of coin: ");
            char input = Console.ReadLine().ToLower().ToCharArray()[0];

            Coin coin = Coin.Unknown;

            // check what type of coin the user entered and save the equivalent enum
            if (input == 'p')
            {
                coin = Coin.Penny;
            }
            else if (input == 'n')
            {
                coin = Coin.Nickel;
            }
            else if (input == 'd')
            {
                coin = Coin.Dime;
            }
            else if (input == 'q')
            {
                coin = Coin.Quarter;
            }

            // check the enum and display the value of the equivalent coin
            switch (coin)
            {
                case Coin.Penny:
                    Console.WriteLine("A penny is worth $0.01");
                    break;
                case Coin.Nickel:
                    Console.WriteLine("A nickel is worth $0.05");
                    break;
                case Coin.Dime:
                    Console.WriteLine("A dime is worth $0.10");
                    break;
                case Coin.Quarter:
                    Console.WriteLine("A quarter is worth $0.25");
                    break;
                case Coin.Unknown:
                    Console.WriteLine("Sorry, didn't recognize your coin");
                    break;
            }
        }
    }

    internal class Area
    {
        internal static void PrintArea(Shape shape, double length, double width)
        {
            // make sure the shape is valid
            if (shape != Shape.Rectangle && shape != Shape.Square && shape != Shape.Triangle)
            {
                Console.WriteLine("Sorry, your shape wasn't recognized");
                return;
            }

            // calculate area
            double area = length * width;

            // area of a triangle is cut in half
            if (shape == Shape.Triangle)
            {
                area /= 2.0;
            }

            // display the area
            Console.WriteLine($"The area of the {shape.ToString().ToLower()} with length {length:F1} and width {width:F1} is {area:F1}");
        }
    }

}
