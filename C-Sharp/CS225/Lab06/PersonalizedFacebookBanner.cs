﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab06
{
    internal class PersonalizedFacebookBanner : FacebookBanner, IRandomDrawMethods
    {
        // an array of many colors for the random ovals and rectangles
        private Color[] colors = { Color.Blue, Color.Cyan, Color.Green, Color.Magenta, Color.Orange, Color.Pink, Color.Red, Color.Yellow };
        private Random random;

        internal PersonalizedFacebookBanner() : base()
        {
            // initialize random number generator
            random = new Random();
        }

        /// <summary>
        /// Draw a bitmap with alternating rows of ovals and rotated rectangles of alternating random colors
        /// </summary>
        public override void DrawCustomizedDecoration()
        {
            int i = 0;

            Color[] colors = new Color[3];

            // generate 3 random colors
            for (int x = 0; x < 3; x++)
            {
                int r = random.Next(0, 256);
                int g = random.Next(0, 256);
                int b = random.Next(0, 256);

                colors[x] = Color.FromArgb(r, g, b);
            }

            // fill the entire image with ovals and rectangles
            for (int x = 0; x < bitmap.Width; x += 10)
            {
                for (int y = 0; y < bitmap.Height; y += 10)
                {
                    // pick one of the three colors
                    solidBrush.Color = colors[i++ % 3];

                    // checking if i is even will flip between ovals and rectangles every line
                    if (i % 2 == 0)
                    {
                        // draw an oval
                        graphics.FillEllipse(solidBrush, x, y, 10, 10);
                    }
                    else
                    {
                        // create a new rectangle
                        Rectangle rectangle = new Rectangle(x, y, 10, 10);

                        // rotation using matrix
                        using (Matrix matrix = new Matrix())
                        {
                            // rotate at a random angle from whatever point the rectangle is at
                            matrix.RotateAt(random.Next(181), new PointF(rectangle.Left + (rectangle.Width / 2), rectangle.Top + (rectangle.Height / 2)));
                            // set the transform
                            graphics.Transform = matrix;
                            // draw the rectangle
                            graphics.FillRectangle(solidBrush, rectangle);
                            // reset the transform
                            graphics.ResetTransform();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Draw an oval with a random size, color, and location
        /// </summary>
        public void DrawRandomFilledOval()
        {
            // get random position
            int x = random.Next(Width + 1);
            int y = random.Next(Height + 1);

            // get random size
            int width = random.Next(101);
            int height = random.Next(101);

            // set random color
            solidBrush.Color = colors[random.Next(colors.Length)];

            // draw randomized oval
            graphics.FillEllipse(solidBrush, x, y, width, height);
        }

        /// <summary>
        /// Draw a rectangle with a random size, color, and location
        /// </summary>
        public void DrawRandomFilledRectangle()
        {
            // get random position
            int x = random.Next(Width + 1);
            int y = random.Next(Height + 1);

            // get random size
            int width = random.Next(101);
            int height = random.Next(101);

            // set random color
            solidBrush.Color = colors[random.Next(colors.Length)];

            // draw randomized rectangle
            graphics.FillRectangle(solidBrush, x, y, width, height);
        }

        /// <summary>
        /// Draws a string to a bitmap image at a random location, size, style, and font family
        /// </summary>
        /// <param name="text">The string to draw on the bitmap</param>
        public void DrawTextAtRandomLocation(string text)
        {
            // array of font styles
            FontStyle[] fontStyle = { FontStyle.Bold, FontStyle.Italic, FontStyle.Regular, FontStyle.Strikeout, FontStyle.Underline };
            // array of font families
            FontFamily[] family = { (new FontFamily("Times New Roman")), (new FontFamily("Comic Sans MS")), (new FontFamily("Arial")) };

            // generate a new font with a random family and style and size
            Font font = new Font(family[random.Next(family.Length)], random.Next(12, 30), fontStyle[random.Next(fontStyle.Length)]);

            // draw the text in black
            solidBrush.Color = Color.Black;

            // have to rotate the text using a matrix
            using (Matrix matrix = new Matrix())
            {
                // generate a random rotation starting at a random point
                matrix.RotateAt(random.Next(181), new PointF(random.Next(Width), random.Next(Height)));
                // set the transform
                graphics.Transform = matrix;
                // draw the string at a random location
                graphics.DrawString(text, font, solidBrush, random.Next(Width), random.Next(Height));
                // reset the transform
                graphics.ResetTransform();
            }
        }
    }
}
