﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab06
{
    class Program
    {
        // change with whatever file paths
        private static readonly string picture = "C:\\Users\\ekirschenmann\\source\\repos\\CS-Projects\\C-Sharp\\CS225\\Lab06\\myPic.jpg";
        private static readonly string output = "C:\\Users\\ekirschenmann\\source\\repos\\CS-Projects\\C-Sharp\\CS225\\Lab06\\kirschenmann";

        private static void Main(string[] args)
        {
            CreatePsychadelicBanner();
            CreateCustomBanner();
        }

        private static void CreatePsychadelicBanner()
        {
            PersonalizedFacebookBanner banner = new PersonalizedFacebookBanner();

            // generate a bunch of rectangles and ovals
            for (int i = 0; i < 30000; i++)
            {
                banner.DrawRandomFilledOval();
                banner.DrawRandomFilledRectangle();
            }

            // write a bunch of strings to the image
            for (int i = 0; i < 20; i++)
            {
                banner.DrawTextAtRandomLocation("CS is fun");
                banner.DrawTextAtRandomLocation("Cool");
                banner.DrawTextAtRandomLocation("Awesome");
                banner.DrawTextAtRandomLocation("So Fun!");
            }

            // draw the center image then save the image to file
            banner.DrawCenteredPicture(picture, 50);
            banner.WriteImageToFile(output + "_Psych_FBB");
        }

        private static void CreateCustomBanner()
        {
            // create a custom banner
            PersonalizedFacebookBanner fbb = new PersonalizedFacebookBanner();
            fbb.DrawCustomizedDecoration();

            // draw center image then save the image to file
            fbb.DrawCenteredPicture(picture, 50);
            fbb.WriteImageToFile(output + "_Custom_FBB");
        }
    }
}
