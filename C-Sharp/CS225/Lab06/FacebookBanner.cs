﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab06
{
    internal abstract class FacebookBanner
    {
        // the height and width of the image
        private readonly int _height = 315;
        private readonly int _width = 850;

        // objects used for generating the bitmap and drawing
        internal Bitmap bitmap;
        internal SolidBrush solidBrush;
        internal Graphics graphics;

        public FacebookBanner()
        {
            // initialize objects for creating new image
            bitmap = new Bitmap(_width, _height);
            solidBrush = new SolidBrush(Color.White);
            graphics = Graphics.FromImage(bitmap);

            // fill bitmap with white background to start
            graphics.FillRectangle(solidBrush, 0, 0, _width, _height);
        }

        /// <summary>
        /// Property giving access to the height
        /// </summary>
        protected int Height => _height;

        /// <summary>
        /// Property giving access to the width
        /// </summary>
        protected int Width => _width;

        /// <summary>
        /// Draw the provided image to the center of the bitmap at the provided size
        /// </summary>
        /// <param name="filename">Path to an image file</param>
        /// <param name="percentSize">The size to scale the image to as an int (i.e. 50% -> 50)</param>
        public void DrawCenteredPicture(string filename, int percentSize)
        {
            Image image = Image.FromFile(filename);

            // calculate how big the image should be
            int w = (int)(image.Width * (percentSize / 100.0));
            int h = (int)(image.Height * (percentSize / 100.0));

            // calculate the location of where to draw the image
            int x = ((bitmap.Width / 2) - (w / 2));
            int y = ((bitmap.Height / 2) - (h / 2));

            // draw 10px white border around image
            solidBrush.Color = Color.White;
            graphics.FillRectangle(solidBrush, (x - 10), (y - 10), (w + 20), (h + 20));

            // draw the image
            graphics.DrawImage(image, x, y, w, h);
        }

        public abstract void DrawCustomizedDecoration();

        /// <summary>
        /// Save the bitmap to file as a png
        /// </summary>
        /// <param name="filename">Path to save the image to</param>
        public void WriteImageToFile(string filename)
        {
            try
            {
                // save image to file
                bitmap.Save(filename + ".png", ImageFormat.Png);

                // clean up resources
                solidBrush.Dispose();
                graphics.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
