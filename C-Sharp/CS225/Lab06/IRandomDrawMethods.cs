﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab06
{
    public interface IRandomDrawMethods
    {
        void DrawRandomFilledOval();
        void DrawRandomFilledRectangle();
        void DrawTextAtRandomLocation(string text);
    }
}
