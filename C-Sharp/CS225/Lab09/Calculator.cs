﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab09
{
    public partial class Calculator : Form
    {
        // values to store inputs
        private int num1;
        private int num2;

        public Calculator()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Operate the addition function
        /// </summary>
        /// <param name="sender">Object that called method</param>
        /// <param name="e">Bundle of information about sender</param>
        private void AddButton_Click(object sender, EventArgs e)
        {
            // set operator text
            operatorLabel.Text = "+";

            // get values
            num1 = (int)numericUpDown1.Value;
            num2 = (int)numericUpDown2.Value;

            // calculate
            numericUpDown3.Value = num1 + num2;
        }

        /// <summary>
        /// Operate the subtraction function
        /// </summary>
        /// <param name="sender">Object that called method</param>
        /// <param name="e">Bundle of information about sender</param>
        private void SubtractButton_Click(object sender, EventArgs e)
        {
            // set operator text
            operatorLabel.Text = "-";

            // get values
            num1 = (int)numericUpDown1.Value;
            num2 = (int)numericUpDown2.Value;

            // calculate
            numericUpDown3.Value = num1 - num2;
        }

        /// <summary>
        /// Operate the multiplication function
        /// </summary>
        /// <param name="sender">Object that called method</param>
        /// <param name="e">Bundle of information about sender</param>
        private void MultiplyButton_Click(object sender, EventArgs e)
        {
            // set operator text
            operatorLabel.Text = "×";

            // get values
            num1 = (int)numericUpDown1.Value;
            num2 = (int)numericUpDown2.Value;

            // calculate
            numericUpDown3.Value = num1 * num2;
        }

        /// <summary>
        /// Operate the divide function
        /// </summary>
        /// <param name="sender">Object that called method</param>
        /// <param name="e">Bundle of information about sender</param>
        private void DivideButton_Click(object sender, EventArgs e)
        {
            // set operator text
            operatorLabel.Text = "÷";

            // get values
            num1 = (int)numericUpDown1.Value;
            num2 = (int)numericUpDown2.Value;

            // don't divide by zero
            if (num2 != Decimal.Zero)
            {
                // calculate
                numericUpDown3.Value = num1 / num2;
            }
            else
            {
                // tell user not to divide by zero
                MessageBox.Show("Cannot divide by zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Selects text within NumericUpDown when in focus
        /// </summary>
        /// <param name="sender">Object that called method</param>
        /// <param name="e">Bundle of information about sender</param>
        private void NumericUpDown_Enter(object sender, EventArgs e)
        {
            // get NumericUpDown and select entire current value
            if (sender is NumericUpDown numeric)
            {
                numeric.Select(0, numeric.Value.ToString().Length);
            }
        }
    }
}
