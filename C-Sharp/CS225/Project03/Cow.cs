﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project03
{
    class Cow : Animal
    {
        public Cow(int numberOfRequiredFeedings) : base(numberOfRequiredFeedings) { }

        public override void Feed(FoodType foodType)
        {
            if (foodType == FoodType.Grass)
            {
                _numberOfFeedings++;
            }
        }

        public override string Speak()
        {
            return "Moo";
        }

        public override string ToString()
        {
            return "cow";
        }
    }
}
