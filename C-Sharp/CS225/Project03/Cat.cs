﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project03
{
    class Cat : Animal
    {
        public Cat(int numberOfRequiredFeedings) : base(numberOfRequiredFeedings) { }

        public override void Feed(FoodType foodType)
        {
            if (foodType == FoodType.Salmon)
            {
                _numberOfFeedings++;
            }
        }

        public override string Speak()
        {
            return "Meow";
        }

        public override string ToString()
        {
            return "cat";
        }
    }
}
