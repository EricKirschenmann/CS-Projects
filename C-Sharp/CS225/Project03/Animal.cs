﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project03
{
    class Animal
    {
        protected internal int _numberOfFeedings;
        private int _numberOfRequiredFeedings;

        public Animal(int numberOfRequiredFeedings)
        {
            _numberOfFeedings = 0;
            _numberOfRequiredFeedings = numberOfRequiredFeedings;
        }

        public virtual void Feed(FoodType foodType)
        {
            Console.WriteLine("THIS SHOULD HAVE BEEN OVERRIDDEN");
        }

        public virtual string Speak()
        {
            Console.WriteLine("THIS SHOULD HAVE BEEN OVERRIDDEN");
            return null;
        }

        public bool StillHungry()
        {
            return _numberOfFeedings < _numberOfRequiredFeedings;
        }
    }
}
