﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project03
{
    class Dog : Animal
    {
        public Dog(int numberOfRequiredFeedings) : base(numberOfRequiredFeedings) { }

        public override void Feed(FoodType foodType)
        {
            if (foodType == FoodType.Bone)
            {
                _numberOfFeedings++;
            }
        }

        public override string Speak()
        {
            return "Woof";
        }

        public override string ToString()
        {
            return "dog";
        }
    }
}
