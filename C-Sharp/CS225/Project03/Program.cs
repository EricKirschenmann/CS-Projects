﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project03
{
    class Program
    {
        static void Main(string[] args)
        {
            int numAnimals = 6;

            List<Animal> stalls = generateRandomStallArrangement(numAnimals);
            // Use this test case (comment out line above) to generate random test cases
            //List<Animal> stalls = generateDogCatCow5ServingArrangement();

            int[] food = new int[3];
            int[] animals = new int[3];

            // step through the List and feed the Animals and count the feedings and animals
            foreach (Animal animal in stalls)
            {
                string s = animal.Speak();

                // check which animal
                switch (s)
                {
                    case "Meow":
                        animals[0]++;
                        while (animal.StillHungry())
                        {
                            animal.Feed(FoodType.Salmon);
                            food[0]++;
                        }
                        break;
                    case "Moo":
                        animals[1]++;
                        while (animal.StillHungry())
                        {
                            animal.Feed(FoodType.Grass);
                            food[1]++;
                        }
                        break;
                    case "Woof":
                        animals[2]++;
                        while (animal.StillHungry())
                        {
                            animal.Feed(FoodType.Bone);
                            food[2]++;
                        }
                        break;
                }
            }

            double total;

            // check the data
            for (int x = 0; x < stalls.Count; x++)
            {
                Animal animal = stalls[x];
                total = 0.0;

                // get the total cost of each animal
                if (animal is Dog)
                {
                    total = animal._numberOfFeedings * FoodType.Bone.GetPrice();
                }
                else if (animal is Cat)
                {
                    total = animal._numberOfFeedings * FoodType.Salmon.GetPrice();
                }
                else if (animal is Cow)
                {
                    total = animal._numberOfFeedings * FoodType.Grass.GetPrice();
                }

                // display the information about the stalls
                Console.WriteLine($"Stall {x + 1} of {stalls.Count} contains a {animal.ToString()} who had {animal._numberOfFeedings} feedings, which cost {total:C2}");
            }


            // calculate the total cost
            total = food[0] * FoodType.Salmon.GetPrice() + food[1] * FoodType.Grass.GetPrice() + food[2] * FoodType.Bone.GetPrice();

            // display the total cost per animal and total cost of the feedings
            Console.WriteLine();
            Console.WriteLine("Old MacDonald's Expenses:");
            Console.WriteLine($"{(food[2] * FoodType.Bone.GetPrice()):C2} spent feeding {animals[2]} dog(s) {food[2]} total feedings.");
            Console.WriteLine($"{(food[0] * FoodType.Salmon.GetPrice()):C2} spent feeding {animals[0]} cat(s) {food[0]} total feedings.");
            Console.WriteLine($"{(food[1] * FoodType.Grass.GetPrice()):C2} spent feeding {animals[1]} cow(s) {food[1]} total feedings.");
            Console.WriteLine($"\tTotal Cost: {total:C2}");

            // Exiting program
            Console.WriteLine();
            Console.WriteLine("E-I-E-I-O!");
        }

        /// <summary>
        /// Generates a stall with the provided number of animals which are generated randomly.
        /// In addition, the number of feedings required per animal are set randomly as
        /// a number between 1 and 10.
        /// </summary>
        /// <param name="numAnimals">How many animals to fill the stall with</param>
        /// <returns>A List with randomly generated animals</returns>
        private static List<Animal> generateRandomStallArrangement(int numAnimals)
        {
            // Create new stall (ArrayList) of animals
            List<Animal> newStallArrangement = new List<Animal>();

            // Generate random animal  numbers of feedings required
            // AND random number of feedings required per animal
            Random r = new Random();

            // Generate numAnimals new animals
            for (int i = 0; i < numAnimals; i++)
            {
                int randAnimal = r.Next(3); // (Dog = 0, Cat = 1, Cow = 2)
                int numFeedings = r.Next(10) + 1; // 1-10 feedings required per animal

                if (randAnimal == 0)
                {
                    newStallArrangement.Add(new Dog(numFeedings));
                }
                else if (randAnimal == 1)
                {
                    newStallArrangement.Add(new Cat(numFeedings));
                }
                else
                {
                    newStallArrangement.Add(new Cow(numFeedings));
                }
            }

            return newStallArrangement;
        }

        /// <summary>
        /// Creates an ArrayList with one of each animal type with 5 feedings required
        /// </summary>
        /// <returns>An ArrayList with one of each animal requiring 5 feedings</returns>
        private static List<Animal> generateDogCatCow5ServingArrangement()
        {
            // Create new stall (ArrayList) of animals containing a Dog, Cat & Cow, each with a requirement of 5 servings
            List<Animal> newStallArrangement = new List<Animal>();
            newStallArrangement.Add(new Dog(5));
            newStallArrangement.Add(new Cat(5));
            newStallArrangement.Add(new Cow(5));
            return newStallArrangement;
        }
    }
}