﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project03
{
    enum FoodType
    {
        Bone,
        Grass,
        Salmon
    }

    internal static class Extensions
    {
        internal static double GetPrice(this FoodType food)
        {
            switch (food)
            {
                case FoodType.Salmon:
                    return 5.0;
                case FoodType.Bone:
                    return 3.0;
                case FoodType.Grass:
                    return 1.0;
                default:
                    return 0.0;
            }
        }
    }
}
