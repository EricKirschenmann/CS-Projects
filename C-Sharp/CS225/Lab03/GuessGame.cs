﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Lab03
{
    class GuessGame
    {
        // a 3x3x3 array that contains the three game boards
        private int[,,] _boards = new int[3, 3, 3];

        // the board that the user chose
        private int choice;

        // the user's running score
        private int score = 0;

        internal GuessGame()
        {
            RunGame();
        }

        void RunGame()
        {
            // get the number of turns the user wants to play
            Console.Write("How many turns do you want to play: ");
            int turns = int.Parse(Console.ReadLine());

            // go through the turns
            for (int i = 0; i < turns; i++)
            {
                // display current turn info
                Console.WriteLine();
                Console.WriteLine($"----------Turn #{(i + 1)}----------");
                Console.WriteLine($"Your current score: {score}");

                // set up the boards for this round
                CreateBoards();
                PrintBoards();

                // reset the user's choice
                choice = -1;

                // get how many seconds remained when the user made their guess
                int time = DisplayCountDown();

                // check if they made the right choice
                AnalyzeChoice(time);

                // give some time to view results of the turn
                Thread.Sleep(2500);
            }

            // display the final score now that the game has ended
            Console.WriteLine();
            Console.WriteLine($"Final Score: {score}");
            Console.WriteLine("Thanks for playing!");
        }

        /// <summary>
        /// Starts a background thread that is waiting for the user to enter a number from 1 to 3 in the console window.
        /// While that thread is running the Console displays a countdown timer for 10 seconds. When the user makes a choice,
        /// the countdown ends and returns the number of seconds that were remaining when the user made a choice
        /// </summary>
        /// <returns>An int containing the number of seconds remaining when the user chose a board</returns>
        int DisplayCountDown()
        {
            // create and start a new thread that will wait for user input
            new Thread(() =>
            {
                // make it a background thread
                Thread.CurrentThread.IsBackground = true;

                // keep getting input until its valid
                do
                {
                    try
                    {
                        // get a choice from the user
                        choice = int.Parse(Console.ReadKey().KeyChar.ToString());
                    }
                    catch (FormatException)
                    {
                        // nothing ignore non-numbers
                    }
                } while (choice < 1 || choice > 3);

            }).Start();

            // reset the time
            int time = -1;

            // begin the countdown timer (start at 11 because there was no way to guess with 10 seconds left before)
            for (int x = 11; x > 0; x--)
            {
                // count down every second (just display that they are in second 10 not 11 or 9 not 10
                Console.WriteLine((x - 1) + "...");
                Thread.Sleep(1000);

                // valid input found exit countdown
                if (choice >= 1 && choice <= 3)
                {
                    // if a valid choice get the remaining time and exit the countdown
                    time = x - 1;
                    break;
                }
            }

            // return the remaining time
            return time;
        }

        /// <summary>
        /// Totals up the random numbers in each of the three game boards, finds the highest total out of the boards,
        /// then checks if the user chose the board with the highest total. If they did add the remaining time to the score,
        /// if not they receive no points
        /// </summary>
        /// <param name="time">The seconds remaining when the user chose a board, will be added to score if they chose correctly</param>
        void AnalyzeChoice(int time)
        {
            // display the user's choice for this round
            Console.WriteLine();
            Console.WriteLine($"You guessed board {choice} with {time} seconds left.");

            // holds the highest board total for this round
            int max = -1;
            // holds the total for each board
            int[] boardTotals = new int[3];

            // loop through each row and column of each board and total up their numbers and find the highest score
            for (int x = 0; x < _boards.GetLength(0); x++)
            {
                for (int y = 0; y < _boards.GetLength(1); y++)
                {
                    for (int z = 0; z < _boards.GetLength(2); z++)
                    {
                        // total each number in the board
                        boardTotals[x] += _boards[x, y, z];
                    }
                }

                // see if current board has a higher value than max
                max = Math.Max(max, boardTotals[x]);
            }

            // print out each of the board totals
            for (int x = 0; x < boardTotals.Length; x++)
            {
                Console.Write($"Board {(x + 1)} Total: {boardTotals[x]}");

                // this is the board the user chose, display right or wrong
                if (x == (choice - 1))
                {
                    // display whether the board they guessed has the max score
                    if (boardTotals[x] == max)
                    {
                        Console.Write(" (Right)");
                        score += time;
                    }
                    else
                    {
                        // they got correct, add the remaining time to their score
                        Console.Write(" (Wrong)");
                    }
                }

                Console.WriteLine();
            }
        }

        /// <summary>
        /// Goes through the 3x3x3 array that serves as the three game boards and fills them with random numbers from 1 to 9
        /// </summary>
        void CreateBoards()
        {
            Random random = new Random();
            for (int x = 0; x < _boards.GetLength(0); x++)
            {
                for (int y = 0; y < _boards.GetLength(1); y++)
                {
                    for (int z = 0; z < _boards.GetLength(2); z++)
                    {
                        // give this element a random number
                        _boards[x, y, z] = random.Next(1, 9);
                    }
                }
            }
        }

        /// <summary>
        /// Writes the three game boards to the console
        /// </summary>
        void PrintBoards()
        {
            for (int x = 0; x < _boards.GetLength(0); x++)
            {
                // display which board is being printed
                Console.WriteLine($"Board #{(x + 1)}");
                for (int y = 0; y < _boards.GetLength(1); y++)
                {
                    for (int z = 0; z < _boards.GetLength(2); z++)
                    {
                        Console.Write(_boards[x, y, z] + " ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
        }
    }


}
