﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Lab03
{
    class Program
    {

        static void Main(string[] args)
        {

            GuessGame game = new GuessGame();



            //Tuple<int, int> guess = GetChoiceFromThread();

            //Console.WriteLine();
            //Console.WriteLine($"You chose board {guess.Item1} with {guess.Item2} seconds left.");
        }

        /// <summary>
        /// Starts a background thread then displays a countdown from 10 
        /// while waiting for the background thread to read in a valid user input
        /// </summary>
        /// <returns>A Tuple containing two integers, first is the the number read in from the thread, second is the time remaining when the guess was made</returns>
        static Tuple<int, int> GetChoiceFromThread()
        {
            int choice = -1;

            // create and start a new thread that will wait for user input
            new Thread(() =>
            {
                // make it a background thread
                Thread.CurrentThread.IsBackground = true;

                // keep getting input until its valid
                do
                {
                    try
                    {
                        // get a choice from the user
                        choice = int.Parse(Console.ReadKey().KeyChar.ToString());
                    }
                    catch (FormatException)
                    {
                        // nothing ignore non-numbers
                    }
                } while (choice < 1 || choice > 3);

            }).Start();

            int time = -1;

            // begin the countdown timer
            for (int x = 10; x > 0; x--)
            {
                // count down every second
                Console.WriteLine(x + "...");
                Thread.Sleep(1000);

                // valid input found exit countdown
                if (choice >= 1 && choice <= 3)
                {
                    time = x;
                    break;
                }
            }

            // return the choice and the remaining time
            return new Tuple<int, int>(choice, time);
        }
    }
}
