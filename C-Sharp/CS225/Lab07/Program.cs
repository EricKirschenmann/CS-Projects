﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab07
{
    class Program
    {
        private static string[] _daysOfWeek = { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };

        static void Main(string[] args)
        {
            CheckDayOfWeek();
            CheckPassword();
        }

        /// <summary>
        /// Gets a number between 1 and 7 from the user and checks what day it is, handles exceptions
        /// </summary>
        private static void CheckDayOfWeek()
        {
            Console.Write("Please enter a number between 1 and 7: ");
            while (true)
            {
                try
                {
                    // get the user's selected day
                    int day = int.Parse(Console.ReadLine());
                    try
                    {
                        // if valid let the user know which day they selected
                        Console.WriteLine($"You've selected {_daysOfWeek[day - 1]}");
                        break; // break out of the loop
                    }
                    catch (IndexOutOfRangeException)
                    {
                        // if the input is not within the bounds of the array get another input
                        Console.Write("Sorry, please enter a number between 1 and 7: ");
                    }
                }
                catch (FormatException)
                {
                    // if the input isn't a valid integer get another input
                    Console.Write("Sorry, please enter a number between 1 and 7: ");
                }
            }
        }

        /// <summary>
        /// Gets two passwords from the user and checks whether they are valid, handles exceptions
        /// </summary>
        private static void CheckPassword()
        {
            Password password;
            // continuously get a password until it is valid
            while (true)
            {
                // make sure no IllegalPasswordExceptions are thrown
                try
                {
                    Console.Write("Please enter your password: ");
                    String password1 = Console.ReadLine();
                    Console.Write("Please re-enter your password: ");
                    String password2 = Console.ReadLine();
                    // attempt to set the password
                    password = new Password(password1, password2);
                    break;
                }
                catch (IllegalPasswordException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            // finally display the password
            Console.WriteLine($"Your password is {password.StoredPassword}");
        }
    }
}
