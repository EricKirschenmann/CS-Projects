﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab07
{
    class IllegalPasswordException : Exception
    {
        public IllegalPasswordException() : base()
        {
        }

        public IllegalPasswordException(string message) : base(message)
        {
        }
    }
}
