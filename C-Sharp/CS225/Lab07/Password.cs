﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab07
{
    class Password
    {
        // contains the successfully created password
        private string _storedPassword;

        /// <summary>
        /// Property that returns the _storedPassword variable
        /// </summary>
        internal string StoredPassword { get; }

        /// <summary>
        /// Constructor that checks that the passwords entered are valid and sets the password
        /// </summary>
        /// <param name="password1">The first password entered</param>
        /// <param name="password2">The second password entered</param>
        internal Password(string password1, string password2)
        {
            bool digit = false;
            bool upper = false;
            bool lower = false;

            // check that all rules are met
            if (password1.Equals(password2))
            {
                for (int x = 0; x < password1.Length; x++)
                {
                    if (Char.IsNumber(password1[x]))
                    {
                        digit = true;
                    }
                    else if (Char.IsUpper(password1[x]))
                    {
                        upper = true;
                    }
                    else if (Char.IsLower(password1[x]))
                    {
                        lower = true;
                    }
                }
            }
            else
            {
                throw new IllegalPasswordException("Passwords must match");
            }

            // throw exceptions if the rules are not met
            if (!digit)
            {
                throw new IllegalPasswordException("Passwords must contain a digit!!");
            }
            if (!upper)
            {
                throw new IllegalPasswordException("Passwords must contain an uppercase letter!!");
            }
            if (!lower)
            {
                throw new IllegalPasswordException("Passwords must conatin a lowercase letter!!");
            }

            // if all rules met set the password
            _storedPassword = password1;
        }
    }
}
