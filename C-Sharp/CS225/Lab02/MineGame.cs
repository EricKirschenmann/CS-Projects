﻿using System;

namespace Lab02
{
    internal class MineGame
    {
        // contains the gameboard
        private MineCell[,] _gameBoard;

        // whether the game is over or not, or if a space has already been guessed
        private bool _gameOver;
        private bool _repeat;

        // constants for points
        private const int POINT_BONUS = 1;
        private const int POINT_PENALTY = 4;

        // how many empty cells to find
        private int totalEmpty;

        internal MineGame()
        {
            InitializeGame();
            PlayGame();
        }

        private void InitializeGame()
        {
            // initialize all the starting variables
            _gameOver = false;
            _repeat = false;
            GenerateBoard(5, 5);
        }

        private void PlayGame()
        {
            // variables that keep track of everything
            int score = 5;
            int turn = 1;
            string result = "";
            int empty = 0;

            while (!_gameOver)
            {
                // print the current turns info and board
                PrintTurnInfo(turn, score, result);

                // get a guess and see if it hit or not
                if (CheckGuess(MakeGuess()))
                {
                    // if the user guessed the same space as before
                    if (_repeat)
                    {
                        // let the user know
                        result = "You've already guessed that!";
                        _repeat = false;
                        continue;
                    }

                    // hit an empty space give points and count
                    score += POINT_BONUS;
                    result = "You hit an empty space!";
                    empty++;

                    // game continues until you find all 20 empty spots
                    if (empty >= 20)
                    {
                        // game is now over
                        _gameOver = true;
                        result = "Congratulations you've won!";
                    }
                }
                else
                {
                    // guess ended up hitting a mine, remove points
                    score -= POINT_PENALTY;
                    result = "You hit a mine!";

                    // game continues until you lose all your points
                    if (score <= 0)
                    {
                        // game is now over
                        _gameOver = true;
                        result = "Sorry you lost!";
                    }
                }

                // next turn
                turn++;
            }

            // print final info and board
            PrintTurnInfo(turn, score, result);
        }

        private static int[] MakeGuess()
        {
            // get a valid row and column guess
            int row = GetPostiveInteger("Please enter a row: ");
            int col = GetPostiveInteger("Please enter a col: ");

            return new int[] { row, col };
        }

        private bool CheckGuess(int[] guess)
        {
            // get the guessed cell type
            MineCell guessCell = _gameBoard[guess[0], guess[1]];

            // check type of cell guessed
            if (guessCell == MineCell.HE)
            {
                // found an empty space
                _gameBoard[guess[0], guess[1]] = MineCell.RE;
                return true;
            }
            else if (guessCell == MineCell.HM)
            {
                // found a mine
                _gameBoard[guess[0], guess[1]] = MineCell.RM;
                return false;
            }
            else
            {
                // guessed this cell already
                _repeat = true;
                return true;
            }
        }

        private static int GetPostiveInteger(string prompt)
        {
            int output = -1;

            // looking for a number between zero and 4
            while (output < 0 || output > 4)
            {
                // get a number from the user
                Console.Write(prompt);

                // attempt to parse an integer out of the user's input
                if (!int.TryParse(Console.ReadLine(), out output))
                {
                    // if it failed to convert, usually text input so invalid
                    output = -1;
                }
            }

            // return the number
            return output;
        }

        private void PrintTurnInfo(int turn, int score, string result)
        {
            // print score and results
            Console.WriteLine();
            Console.WriteLine($"------------------- Turn {turn} -------------------");
            Console.WriteLine(result);
            Console.WriteLine($"Current Score: {score}");
            PrintBoard();
        }

        private void PrintBoard()
        {
            // display the column numbers
            for (int i = 0; i < _gameBoard.GetLength(1); i++)
            {
                Console.Write("\t" + i);
            }

            Console.WriteLine();

            // loop through and print out the entire board
            for (int x = 0; x < _gameBoard.GetLength(0); x++)
            {
                // print current row number
                Console.Write(x + "\t");

                for (int y = 0; y < _gameBoard.GetLength(1); y++)
                {
                    if (_gameBoard[x, y] == MineCell.HE)
                    {
                        // if game isn't over keep cell hidden
                        Console.Write((_gameOver) ? "e\t" : "-\t");
                    }
                    else if (_gameBoard[x, y] == MineCell.HM)
                    {
                        // if game isn't over keep cell hidden
                        Console.Write((_gameOver) ? "m\t" : "-\t");
                    }
                    else if (_gameBoard[x, y] == MineCell.RE)
                    {
                        Console.Write("E\t");
                    }
                    else if (_gameBoard[x, y] == MineCell.RM)
                    {
                        Console.Write("M\t");
                    }
                }

                Console.WriteLine();
            }
        }

        private void ClearBoard()
        {
            // loop through board setting all to empty
            for (int x = 0; x < _gameBoard.GetLength(0); x++)
            {
                for (int y = 0; y < _gameBoard.GetLength(1); y++)
                {
                    _gameBoard[x, y] = MineCell.HE;
                }
            }
        }

        private void GenerateBoard(int row, int col)
        {
            // instantiate board and fill with empty cells
            _gameBoard = new MineCell[row, col];
            ClearBoard();

            // a fifth of the cells will be mines
            int mines = (int)row * col / 5;

            // calculate how many empty cells to find
            totalEmpty = (row * col) - mines;

            // random
            Random random = new Random((int)DateTimeOffset.Now.ToUnixTimeMilliseconds());

            // make sure to set enough mines, don't miss any by random chance
            while (mines > 0)
            {
                for (int x = 0; x < _gameBoard.GetLength(0); x++)
                {
                    for (int y = 0; y < _gameBoard.GetLength(1); y++)
                    {
                        if (!(_gameBoard[x, y] == MineCell.HM))
                        {
                            // get a random number
                            int rand = random.Next(1, (row * col) + 1);

                            // if random chance of mine
                            if (rand % ((int)(row * col / 5)) == 0)
                            {
                                // set cell to mine
                                _gameBoard[x, y] = MineCell.HM;
                                mines--;

                                if (mines == 0)
                                {
                                    // done
                                    return;
                                }
                            }
                        }
                        else
                        {
                            // already a mine
                            continue;
                        }
                    }
                }
            }
        }
    }
}
