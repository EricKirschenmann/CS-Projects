﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project01
{
    class Program
    {
        private static void Main(string[] args)
        {
            // Constructor initializes gradeComputer's array with random values
            GradeBook gradeBook = new GradeBook(5);

            Console.WriteLine(gradeBook.ToString()); // Prints out array, e.g. 38 49 20 89 2

            // Create new array of 5 grades for testing
            // (NOTE: Feel free to change these grades or add new grades to
            //  	  test the functionality of your class)
            int[] newGrades = { 10, 30, 40, 20, 80 };

            // Override grades and print out
            gradeBook.SetGrades(newGrades);
            Console.WriteLine(gradeBook.ToString()); // Now prints: 10 30 40 20 80

            // Print out the number of each letter;
            // e.g., should print out: "0 A's, 1 B's, 0 C's, 0 D's, 4 F's"
            gradeBook.PrintNumLetterGrades(gradeBook.GetLetterGrades());

            // Prints out the total cost of the party
            Console.WriteLine("Time for a $" + gradeBook.ComputePartyCost() + " party!");
        }
    }
}
