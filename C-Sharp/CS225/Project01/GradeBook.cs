﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project01
{
    class GradeBook
    {
        private int[] _grades;
        private int[] _letterGrades;

        internal GradeBook(int students)
        {
            _grades = new int[students];
            _letterGrades = new int[5];

            Random random = new Random();

            // fill array with random numbers
            for (int x = 0; x < students; x++)
            {
                _grades[x] = random.Next(100);
            }
        }

        public int[] GetGrades()
        {
            return _grades;
        }

        public void SetGrades(int[] grades)
        {
            // copy values from supplied array into existing array
            Array.Copy(grades, _grades, _grades.Length);
        }

        public int[] GetLetterGrades()
        {
            // check each grade and count the type
            foreach (int grade in _grades)
            {
                if (grade >= 90)
                {
                    _letterGrades[0]++;
                }
                else if (grade >= 80)
                {
                    _letterGrades[1]++;
                }
                else if (grade >= 70)
                {
                    _letterGrades[2]++;
                }
                else if (grade >= 60)
                {
                    _letterGrades[3]++;
                }
                else
                {
                    _letterGrades[4]++;
                }
            }

            return _letterGrades;
        }

        public void PrintNumLetterGrades(int[] letters)
        {
            Console.WriteLine(letters[0] + " A's, " + letters[1] + " B's, " + letters[2] + " C's, "
                + letters[3] + " D's, " + letters[4] + " F's");
        }

        public int ComputePartyCost()
        {
            int prize = 20;
            int total = 0;
            // calculate the total based on how many A's, B's, and C's were earned
            for (int x = 0; x < 3; x++)
            {
                total += prize * _letterGrades[x];
                prize /= 2; // the prize halves for each lower letter grade
            }
            return total;
        }

        public override string ToString()
        {
            string output = "";
            foreach (int x in _grades)
            {
                output += x + " ";
            }
            return output.Trim(); // remove trailing space
        }
    }

}