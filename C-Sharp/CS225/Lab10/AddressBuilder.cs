﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab10
{
    public partial class AddressBuilder : Form
    {
        public AddressBuilder()
        {
            InitializeComponent();
            prefixComboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Creates the address when the submit button is clicked
        /// </summary>
        /// <param name="sender">Object that called method</param>
        /// <param name="e">Bundle of information about sender</param>
        private void SubmitButton_Click(object sender, EventArgs e)
        {
            // make sure submitted
            if (sender is Button submit)
            {
                // get the different pieces of the address
                string prefix = prefixComboBox.SelectedItem.ToString();
                string firstName = firstNameBox.Text;
                string lastName = lastNameBox.Text;
                string address = addressBox.Text;
                string city = cityBox.Text;
                string state = stateBox.Text;
                string zip = zipCodeBox.Text;

                // build the address using the parts
                StringBuilder builder = new StringBuilder(prefix + " ");
                builder.Append(firstName + " ").Append(lastName + ",").AppendLine();
                builder.AppendLine(address + ", ").Append(city + " ").Append(state + " ").Append(zip);

                // set the text to the address
                outputBox.Text = builder.ToString();

                // if male turn the text blue otherwise pink
                if (maleRadio.Checked)
                {
                    outputBox.ForeColor = Color.Blue;
                }
                else
                {
                    outputBox.ForeColor = Color.HotPink;
                }
            }
        }

        /// <summary>
        /// Select the text within the text box
        /// </summary>
        /// <param name="sender">Object that called method</param>
        /// <param name="e">Bundle of information about sender</param>
        private void TextBox_Selected(object sender, EventArgs e)
        {
            if (sender is TextBox box)
            {
                box.SelectAll();
            }
        }

        /// <summary>
        /// Clicks the submit button when the user hits enter
        /// </summary>
        /// <param name="sender">Object that called method</param>
        /// <param name="e">Bundle of information about sender</param>
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                submitButton.PerformClick();
                // these last two lines will stop the beep sound
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }
    }
}
