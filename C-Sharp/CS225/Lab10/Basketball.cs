﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab10
{
    public partial class Basketball : Form
    {
        // contains available players
        private List<string> availablePlayers = new List<string>()
        {
            "Matthew Dellavedova (G)",
            "Joe Harris (G)",
            "Brendan Haywood (C)",
            "Kyrie Irving (G)",
            "LeBron James (F)",
            "James Jones (F)",
            "Kevin Love (F)",
            "Shawn Marion (F)",
            "Mike Miller (F)",
            "Timofey Mozgov (C)",
            "Kendrick Perkins (C)",
            "Iman Shumpert (G)",
            "J.R. Smith (G)",
            "Tristan Thompson (C)",
            "Anderson Varejao (C)"
        };

        // will contain added players
        private List<string> takenPlayers = new List<string>();

        public Basketball()
        {
            InitializeComponent();

            // add starting players
            AddPlayers(availableListBox, availablePlayers);
        }

        /// <summary>
        /// Adds strings from the provided list to the provided listbox
        /// </summary>
        /// <param name="listBox">ListBox to add the strings to</param>
        /// <param name="list">List of strings to add</param>
        private void AddPlayers(ListBox listBox, List<string> list)
        {
            // remove existing items
            listBox.Items.Clear();

            // add all the players within the list
            foreach (string player in list)
            {
                listBox.Items.Add(player);
            }

            // set selected location if there are players
            if (list.Count != 0)
            {
                listBox.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Checks that the added players are valid then moves them
        /// </summary>
        /// <param name="sender">object that called this method</param>
        /// <param name="e">bundle of arguments</param>
        private void SubmitButton_Click(object sender, EventArgs e)
        {
            // make sure there are actually players to move
            if (availablePlayers.Count != 0)
            {
                // get player to move
                string player = availablePlayers[availableListBox.SelectedIndex];

                if (player.Contains("(G)"))
                {
                    // check if both guards have been added
                    if (!guardCheck1.Checked)
                    {
                        guardCheck1.Checked = true;
                    }
                    else if (!guardCheck2.Checked)
                    {
                        guardCheck2.Checked = true;
                    }
                    else
                    {
                        return;
                    }
                }
                else if (player.Contains("(F)"))
                {
                    // check if both forwards have been added
                    if (!forwardCheck1.Checked)
                    {
                        forwardCheck1.Checked = true;
                    }
                    else if (!forwardCheck2.Checked)
                    {
                        forwardCheck2.Checked = true;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    // check if there is already a center
                    if (!centerCheck.Checked)
                    {
                        centerCheck.Checked = true;
                    }
                    else
                    {
                        return;
                    }
                }

                // enable button if all of the positions are filled
                if (guardCheck1.Checked && guardCheck2.Checked && forwardCheck1.Checked && forwardCheck2.Checked && centerCheck.Checked)
                {
                    startButton.Enabled = true;
                }

                // if it's made it this far move the players around
                takenPlayers.Add(player);
                availablePlayers.Remove(player);

                // update players
                AddPlayers(availableListBox, availablePlayers);
                AddPlayers(currentListBox, takenPlayers);
            }
        }

        /// <summary>
        /// Removes the players from the added box back to the original box
        /// </summary>
        /// <param name="sender">object that called this method</param>
        /// <param name="e">bundle of arguments</param>
        private void ReturnButton_Click(object sender, EventArgs e)
        {
            // make sure there are actually players to move
            if (takenPlayers.Count != 0)
            {
                // get player to move
                string player = takenPlayers[currentListBox.SelectedIndex];

                if (player.Contains("(G)"))
                {
                    // remove the existing guards
                    if (guardCheck2.Checked)
                    {
                        guardCheck2.Checked = false;
                    }
                    else if (guardCheck1.Checked)
                    {
                        guardCheck1.Checked = false;
                    }
                    else
                    {
                        return;
                    }
                }
                else if (player.Contains("(F)"))
                {
                    // remove the existing forwards
                    if (forwardCheck2.Checked)
                    {
                        forwardCheck2.Checked = false;
                    }
                    else if (forwardCheck1.Checked)
                    {
                        forwardCheck1.Checked = false;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    // remove the center
                    if (centerCheck.Checked)
                    {
                        centerCheck.Checked = false;
                    }
                    else
                    {
                        return;
                    }
                }

                // make sure to disable the button if any aren't enabled
                if (!guardCheck1.Checked || !guardCheck2.Checked || !forwardCheck1.Checked || !forwardCheck2.Checked || !centerCheck.Checked)
                {
                    startButton.Enabled = false;
                }


                // if it's made it this far move the players around
                availablePlayers.Add(player);
                takenPlayers.Remove(player);

                // update players
                AddPlayers(availableListBox, availablePlayers);
                AddPlayers(currentListBox, takenPlayers);
            }
        }

        /// <summary>
        /// Start the game
        /// </summary>
        /// <param name="sender">object that called this method</param>
        /// <param name="e">bundle of arguments</param>
        private void StartButton_Click(object sender, EventArgs e)
        {
            // show message to start game
            MessageBox.Show("Let's start the game!", "Let's go");
        }
    }
}
