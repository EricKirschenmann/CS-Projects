﻿namespace Lab10
{
    partial class Basketball
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.availableListBox = new System.Windows.Forms.ListBox();
            this.currentListBox = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.submitButton = new System.Windows.Forms.Button();
            this.returnButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.centerCheck = new System.Windows.Forms.CheckBox();
            this.forwardCheck2 = new System.Windows.Forms.CheckBox();
            this.forwardCheck1 = new System.Windows.Forms.CheckBox();
            this.guardCheck2 = new System.Windows.Forms.CheckBox();
            this.guardCheck1 = new System.Windows.Forms.CheckBox();
            this.startButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(484, 461);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.Controls.Add(this.availableListBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.currentListBox, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(478, 339);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // availableListBox
            // 
            this.availableListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableListBox.FormattingEnabled = true;
            this.availableListBox.Location = new System.Drawing.Point(3, 3);
            this.availableListBox.Name = "availableListBox";
            this.availableListBox.Size = new System.Drawing.Size(185, 333);
            this.availableListBox.TabIndex = 0;
            // 
            // currentListBox
            // 
            this.currentListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentListBox.FormattingEnabled = true;
            this.currentListBox.Location = new System.Drawing.Point(289, 3);
            this.currentListBox.Name = "currentListBox";
            this.currentListBox.Size = new System.Drawing.Size(186, 333);
            this.currentListBox.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.submitButton, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.returnButton, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(194, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(89, 333);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // submitButton
            // 
            this.submitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.submitButton.Location = new System.Drawing.Point(7, 113);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 0;
            this.submitButton.Text = "---->";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // returnButton
            // 
            this.returnButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.returnButton.Location = new System.Drawing.Point(7, 196);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(75, 23);
            this.returnButton.TabIndex = 1;
            this.returnButton.Text = "<----";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.ReturnButton_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.startButton, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 348);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(478, 110);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.centerCheck, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.forwardCheck2, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.forwardCheck1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.guardCheck2, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.guardCheck1, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(289, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(186, 104);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // centerCheck
            // 
            this.centerCheck.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.centerCheck.AutoSize = true;
            this.centerCheck.Enabled = false;
            this.centerCheck.Location = new System.Drawing.Point(27, 77);
            this.centerCheck.Name = "centerCheck";
            this.centerCheck.Size = new System.Drawing.Size(39, 17);
            this.centerCheck.TabIndex = 4;
            this.centerCheck.Text = "C1";
            this.centerCheck.UseVisualStyleBackColor = true;
            // 
            // forwardCheck2
            // 
            this.forwardCheck2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.forwardCheck2.AutoSize = true;
            this.forwardCheck2.Enabled = false;
            this.forwardCheck2.Location = new System.Drawing.Point(120, 42);
            this.forwardCheck2.Name = "forwardCheck2";
            this.forwardCheck2.Size = new System.Drawing.Size(38, 17);
            this.forwardCheck2.TabIndex = 3;
            this.forwardCheck2.Text = "F2";
            this.forwardCheck2.UseVisualStyleBackColor = true;
            // 
            // forwardCheck1
            // 
            this.forwardCheck1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.forwardCheck1.AutoSize = true;
            this.forwardCheck1.Enabled = false;
            this.forwardCheck1.Location = new System.Drawing.Point(27, 42);
            this.forwardCheck1.Name = "forwardCheck1";
            this.forwardCheck1.Size = new System.Drawing.Size(38, 17);
            this.forwardCheck1.TabIndex = 2;
            this.forwardCheck1.Text = "F1";
            this.forwardCheck1.UseVisualStyleBackColor = true;
            // 
            // guardCheck2
            // 
            this.guardCheck2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guardCheck2.AutoSize = true;
            this.guardCheck2.Enabled = false;
            this.guardCheck2.Location = new System.Drawing.Point(119, 8);
            this.guardCheck2.Name = "guardCheck2";
            this.guardCheck2.Size = new System.Drawing.Size(40, 17);
            this.guardCheck2.TabIndex = 1;
            this.guardCheck2.Text = "G2";
            this.guardCheck2.UseVisualStyleBackColor = true;
            // 
            // guardCheck1
            // 
            this.guardCheck1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guardCheck1.AutoSize = true;
            this.guardCheck1.Enabled = false;
            this.guardCheck1.Location = new System.Drawing.Point(26, 8);
            this.guardCheck1.Name = "guardCheck1";
            this.guardCheck1.Size = new System.Drawing.Size(40, 17);
            this.guardCheck1.TabIndex = 0;
            this.guardCheck1.Text = "G1";
            this.guardCheck1.UseVisualStyleBackColor = true;
            // 
            // startButton
            // 
            this.startButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.startButton.Enabled = false;
            this.startButton.Location = new System.Drawing.Point(58, 43);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Start Game";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // Basketball
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Basketball";
            this.Text = "Basketball";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ListBox availableListBox;
        private System.Windows.Forms.ListBox currentListBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.CheckBox centerCheck;
        private System.Windows.Forms.CheckBox forwardCheck2;
        private System.Windows.Forms.CheckBox forwardCheck1;
        private System.Windows.Forms.CheckBox guardCheck2;
        private System.Windows.Forms.CheckBox guardCheck1;
        private System.Windows.Forms.Button startButton;
    }
}