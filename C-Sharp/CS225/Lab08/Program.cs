﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Lab08
{
    class Program
    {
        private const string FileName = "C:\\Users\\ekirschenmann\\source\\repos\\CS-Projects\\C-Sharp\\CS225\\Lab08\\employees.bin";

        static void Main(string[] args)
        {
            // get any existing from file
            List<Employee> employees = ReadFromBinaryFile<List<Employee>>(FileName);

            // initialize if nothing from file
            if (employees == null)
            {
                employees = new List<Employee>();
            }

            int choice = -1;

            do
            {
                // Prompt user for choice
                Console.WriteLine("Options: \n"
                    + "  1. See the list of all employees\n"
                    + "  2. Add new employees");
                Console.Write("Please select from the following options (type number): ");

                try
                {
                    choice = int.Parse(Console.ReadLine());  // ...keep asking them for an int.
                    // Clear extra whitespace left by nextInt()
                }
                catch (FormatException)
                {
                    // If they don't enter an int, advance past the bad input
                }

                // Inform user that their choice was not valid
                if (choice != 1 && choice != 2)
                {
                    Console.WriteLine("Invalid choice: Please try again... ");
                }
            } while (choice != 1 && choice != 2);

            if (choice == 1)
            {
                if (employees.Count > 0)
                {
                    Console.Clear();
                    // display all employees
                    Console.WriteLine("Current Employees: ");
                    foreach (Employee employee in employees)
                    {
                        Console.WriteLine(employee.ToString());
                    }

                    // write existing to file
                    WriteToBinaryFile(FileName, employees, false);
                }
                else
                {
                    Console.WriteLine("Sorry, no employees found. Try adding some.");
                }
            }
            else
            {
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine("------------- ADD EMPLOYEE -------------");

                    // get employee info
                    //Console.WriteLine("Add a new employee: ");
                    Console.Write("Please enter first name: ");
                    String first = Console.ReadLine();
                    Console.Write("Please enter last name: ");
                    String last = Console.ReadLine();
                    double rate = GetDouble("Please enter hourly rate: ");
                    double years = GetDouble("Please enter years with the company: ");

                    // add employee
                    Employee employee = new Employee(first, last, rate, years);
                    employees.Add(employee);

                    char input;

                    do
                    {
                        // see if user wants to continue adding employees
                        Console.Write("Would you like to add any more employees? (y or n): ");
                        input = Console.ReadLine().ToLower().Trim()[0];
                    } while (input != 'y' && input != 'n');

                    // exit the program
                    if (input == 'n')
                    {
                        Console.WriteLine("Thank you.");
                        break;
                    }
                }

                // write employees to file
                WriteToBinaryFile(FileName, employees, false);
            }
        }

        /// <summary>
        /// Checks user input while getting a double
        /// </summary>
        /// <param name="message">String containing prompt for user</param>
        /// <returns>A double from the user</returns>
        static double GetDouble(string message)
        {
            double output = -1;

            // looking for a number zero or above
            while (output < 0)
            {
                // get a number from the user
                Console.Write(message);

                // attempt to parse an double out of the user's input
                if (!double.TryParse(Console.ReadLine(), out output))
                {
                    // if it failed to convert, usually text input so invalid
                    output = -1;
                }
            }

            // return the number
            return output;
        }

        /// <summary>
        /// Writes the given object instance to a binary file.
        /// <para>Object type (and all child types) must be decorated with the [Serializable] attribute.</para>
        /// <para>To prevent a variable from being serialized, decorate it with the [NonSerialized] attribute; cannot be applied to properties.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the binary file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the XML file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Reads an object instance from a binary file.
        /// </summary>
        /// <typeparam name="List">The type of object to read from the binary.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the binary file.</returns>
        public static List<Employee> ReadFromBinaryFile<List>(string filePath)
        {
            try
            {
                using (Stream stream = File.Open(filePath, FileMode.Open))
                {
                    // get list from file
                    var binaryFormatter = new BinaryFormatter();
                    List<Employee> list = (List<Employee>)binaryFormatter.Deserialize(stream);

                    // copy them over, this recreates the uniqueid for each as without it it would mess up badly
                    List<Employee> temp = new List<Employee>();
                    for (int x = 0; x < list.Count; x++)
                    {
                        Employee e = new Employee(list[x].FirstName, list[x].LastName, list[x].HourlyRate, list[x].Years);
                        temp.Add(e);
                    }

                    return temp;
                }
            }
            catch (IOException)
            {
                return null;
            }
        }
    }
}
