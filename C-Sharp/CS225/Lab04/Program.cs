﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    class Program
    {
        static void Main(string[] args)
        {
            DealGame game = new DealGame();

            // keep playing the game until it is over
            while (!game.GameOver)
            {
                game.PlayNextTurn();
            }

            Console.WriteLine("Thank you for playing!");
        }
    }
}
