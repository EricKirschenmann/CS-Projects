﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    internal class DealGame
    {
        // contains the starting prizes
        private readonly double[] _cashPrizes = { .01, .50, 1, 5, 10, 50, 100, 250, 500, 1000, 5000, 10000, 100000, 500000, 1000000 };

        // true if game is over, false otherwise
        private bool _gameOver;

        // contains the bank's previous offers
        private List<double> _offerHistory;

        // contains the remaining prizes
        private List<double> _remainingPrizes;

        // allow other classes to get the _gameOver
        public bool GameOver { get => _gameOver; }

        /// <summary>
        /// Constructor initializes _remainingPrizes to a new list
        /// containing a random ordering of all prizes
        /// </summary>
        internal DealGame()
        {
            // new game
            _gameOver = false;

            // create randomly sorted list of prizes
            InitializeRandomPrizeBoard();

            // initialize list
            _offerHistory = new List<double>();
        }

        /// <summary>
        /// Executes a single turn of the game
        /// </summary>
        internal void PlayNextTurn()
        {
            Console.WriteLine("----------------------------------------");

            // display the remaining prizes
            Console.WriteLine($"There are {_remainingPrizes.Count} prizes remaining, listed in ascending order:");
            PrintRemainingPrizeBoard();

            // display the available door numbers
            Console.WriteLine("\nThe prizes have been dispersed randomly behind the following doors:");
            for (int x = 0; x < _remainingPrizes.Count; x++)
            {
                Console.Write($"{x} ");
            }
            Console.WriteLine();

            // display the banker's offer
            Console.WriteLine($"The banker would like to make you an offer of........{GetBankOffer():C2}");

            // ask the user to make a selection
            Console.WriteLine($"What would you like to do? Enter '-1' to accept the banker's offer, or select on of the doors above (0-{(_remainingPrizes.Count - 1)}):");

            int input = -100;

            // keep getting input until it's valid
            do
            {
                // get user input
                input = int.Parse(Console.ReadLine());

                // display an error if the user entered something invalid
                if (input < -1 || input > _remainingPrizes.Count)
                {
                    Console.WriteLine($"{input} is not a valid selection.");
                }

            } while (input < -1 || input > _remainingPrizes.Count);

            // user chose a door
            ChooseDoor(input);

            Console.WriteLine();
            Console.WriteLine();
        }

        /// <summary>
        /// Takes in the user's selection and processes the result
        /// </summary>
        /// <param name="choice">The choice the user made</param>
        private void ChooseDoor(int choice)
        {
            // get the banker's offer and add it to the history
            double offer = GetBankOffer();
            _offerHistory.Add(offer);

            // if the user selected the banker's offer
            if (choice == -1)
            {
                // game is now over
                _gameOver = true;

                // display accepted offer
                Console.WriteLine($"You've accepted {offer:C2}");

                // display offer history
                PrintOfferHistory(offer);
            }
            else
            {
                // if they chose a door, get the prize and remove it
                double prize = _remainingPrizes[choice];
                _remainingPrizes.Remove(prize);

                // display the prize hidden behind the selected door
                Console.WriteLine($"You've selected door #{choice} which held {prize:C2}.");
            }

            // if the user only has one prize available
            if (_remainingPrizes.Count == 1)
            {
                // game is now over
                _gameOver = true;

                // get the remaining prize
                double finalPrize = _remainingPrizes[0];

                // display the prize that the user won
                Console.WriteLine($"You won the final prize of {finalPrize:C2}");

                // display offer histor
                PrintOfferHistory(finalPrize);
            }
        }

        /// <summary>
        /// Generates the bank's offer. 
        /// 85% of the average of the remaining prizes
        /// </summary>
        /// <returns>The bank's offer based on the remaining prizes</returns>
        private double GetBankOffer()
        {
            double total = 0.0;

            // calculate the total value of the remaining prizes
            foreach (double prize in _remainingPrizes)
            {
                total += prize;
            }

            // return 85% of the average
            return ((total / _remainingPrizes.Count) * 0.85);
        }

        /// <summary>
        /// Copies the prizes into a temporary List then randomly inserts the
        /// into the _remainingPrizes list
        /// </summary>
        void InitializeRandomPrizeBoard()
        {
            // Start with a fresh board with nothing in it
            _remainingPrizes = new List<double>();

            // Copies _cashPrizes array into temporary List
            List<double> prizes = new List<double>();

            // copy the starting prizes
            prizes = _cashPrizes.ToList();

            Random random = new Random();

            // Randomizes prizes into _remainingPrizes
            while (prizes.Any())
            {
                // pick a random remaining prize
                int index = random.Next(0, prizes.Count);
                // copy into our "board"
                _remainingPrizes.Add(prizes[index]);
                // remove copied prize
                prizes.Remove(prizes[index]);
            }
        }

        /// <summary>
        /// Prints out the offers made by the banker in chronological order.Then, prints out the
        /// money left on the table (for example, if the largest offer from the banker was $250,000,
        /// and you ended up winning $1,000, whether from a later banker offer or from the last door,
        /// then you "left $249,000 on the table).
        /// </summary>
        /// <param name="finalPrize">The amount of money won by the user</param>
        private void PrintOfferHistory(double finalPrize)
        {
            double max = double.MinValue;

            // print out the banker offer history
            Console.WriteLine("\n\n\n***BANKER HISTORY***");
            int index = 0;
            foreach (double offer in _offerHistory)
            {
                // find the highest offer
                max = Math.Max(max, offer);
                Console.WriteLine($"Offer {(index + 1)}: {offer:C2}");
                index++;
            }

            // check wether the user won more than the highest offer, won the highest offer, or won less than the highest offer
            if (finalPrize > max)
            {
                Console.WriteLine($"Congrats, you won more than the banker's highest offer of {max:C2}");
            }
            else if (finalPrize == max)
            {
                Console.WriteLine($"Congrats, you won the banker's highest offer of {max:C2}");
            }
            else
            {
                // if they won less than the highest offer, display how much they didn't win
                Console.WriteLine($"Sorry, you left {(max - finalPrize):C2} on the table");
            }
        }

        /// <summary>
        /// Prints the sorted remaining prizes
        /// </summary>
        private void PrintRemainingPrizeBoard()
        {
            List<double> prizes = new List<double>();

            // copy remaining prize to temporary list
            prizes = _remainingPrizes.ToList();

            // sort them
            prizes.Sort();

            // display the remaining prizes
            foreach (double prize in prizes)
            {
                Console.Write($"{prize:C2} ");
            }

            Console.WriteLine();
        }
    }
}
