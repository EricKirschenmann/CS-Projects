﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project02
{
    internal enum GameCell { HE, HS, RE, RS };

    public class BattleShip
    {
        // contains the current game board
        private GameCell[,] _gameBoard = new GameCell[7, 7];
        // contains all the guesses made by the user
        private List<List<int>> _guessHistory = new List<List<int>>();
        // contains whether a guess is a repeat or not
        private bool _repeat;
        // contains whether a game has ended or not
        private bool _gameOver;

        // maintain current turn and number of hits
        private int _turn;
        private int _hits;

        public bool GameOver { get => _gameOver; }

        public BattleShip()
        {
            _gameOver = false;
            ClearBoard();
            SetBoard();
        }

        public void PlayNextTurn()
        {
            // print turn info
            Console.WriteLine();
            Console.WriteLine($"-------------------------- turn {_turn} --------------------------");
            Console.WriteLine("Current board: ");
            PrintBoard();

            // get a guess from the user and check result
            if (CheckGuess(MakeGuess()))
            {
                // make sure the user doesn't reuse a guess
                if (_repeat)
                {
                    _repeat = false;
                    Console.WriteLine("You've already guessed this spot!");
                    return;
                }

                // hit a ship
                Console.WriteLine("It's a hit!");
                _hits++;
            }
            else
            {
                // if a miss tell the user
                Console.WriteLine("It's a miss!");
            }

            // next turn
            _turn++;

            if (_hits >= 9)
            {
                _gameOver = true;
                Console.WriteLine("Game Over! Congrats you've sunk them all!");
                PrintGuessHistory();
            }
        }

        /// <summary>
        /// Get a row and a column from the user
        /// </summary>
        /// <returns></returns>
        private int[] MakeGuess()
        {
            // get a valid row and column guess
            int row = GetPostiveInteger("Please enter a row: ");
            int col = GetPostiveInteger("Please enter a col: ");

            return new int[] { row, col };
        }

        /// <summary>
        /// Fill the game board with only hidden empty cells
        /// </summary>
        private void ClearBoard()
        {
            // fill board with empty spaces
            for (int x = 0; x < _gameBoard.GetLength(0); x++)
            {
                for (int y = 0; y < _gameBoard.GetLength(1); y++)
                {
                    _gameBoard[x, y] = GameCell.HE;
                }
            }
        }

        /// <summary>
        /// See if the user's guess hits a ship or an empty location
        /// </summary>
        /// <param name="guess">an array containing a row and column guess</param>
        /// <returns>true if hit a ship, false if empty space</returns>
        private bool CheckGuess(int[] guess)
        {
            // get the guessed cell type
            GameCell guessCell = _gameBoard[guess[0], guess[1]];

            // check type of cell guessed
            if (guessCell == GameCell.HE)
            {
                // found an empty space
                _gameBoard[guess[0], guess[1]] = GameCell.RE;
                AddGuess(guess);
                return false;
            }
            else if (guessCell == GameCell.HS)
            {
                // found a ship
                _gameBoard[guess[0], guess[1]] = GameCell.RS;
                AddGuess(guess);
                return true;
            }
            else
            {
                // guessed this cell already
                _repeat = true;
                return true;
            }
        }

        /// <summary>
        /// Add a guess into the guess history list
        /// </summary>
        /// <param name="guess">an array containing a row and column guess</param>
        private void AddGuess(int[] guess)
        {
            // create temporary list containing guess
            List<int> temp = new List<int>
            {
                guess[0],
                guess[1]
            };

            // add to the list
            _guessHistory.Add(temp);
        }

        /// <summary>
        /// Prints out row by row the guesses the user made
        /// </summary>
        private void PrintGuessHistory()
        {
            Console.WriteLine("Guess History: ");
            Console.WriteLine("Guess #\t| Row\tCol");
            Console.WriteLine("-------------------");

            // print out all the guesses
            for (int x = 0; x < _guessHistory.Count; x++)
            {
                Console.WriteLine((x + 1) + "\t| " + _guessHistory[x][0] + "\t" + _guessHistory[x][1]);
            }
        }

        /// <summary>
        /// Gets a positive integer between 0 and 6 from the user
        /// </summary>
        /// <param name="prompt">What to display to the user when prompting</param>
        /// <returns></returns>
        private static int GetPostiveInteger(string prompt)
        {
            int output = -1;

            // looking for a number between zero and 6
            while (output < 0 || output > 7)
            {
                // get a number from the user
                Console.Write(prompt);

                // attempt to parse an integer out of the user's input
                if (!int.TryParse(Console.ReadLine(), out output))
                {
                    // if it failed to convert, usually text input so invalid
                    output = -1;
                }
            }

            // return the number
            return output;
        }

        /// <summary>
        /// Prints the current gameboard in a format readable by the user
        /// </summary>
        private void PrintBoard()
        {
            // display the column numbers
            for (int i = 0; i < _gameBoard.GetLength(1); i++)
            {
                Console.Write("\t" + i);
            }

            Console.WriteLine();

            // loop through and print out the entire board
            for (int x = 0; x < _gameBoard.GetLength(0); x++)
            {
                // print current row number
                Console.Write(x + "\t");

                for (int y = 0; y < _gameBoard.GetLength(1); y++)
                {
                    if (_gameBoard[x, y] == GameCell.HE)
                    {
                        // if game isn't over keep cell hidden
                        Console.Write((_gameOver) ? "e\t" : "-\t");
                    }
                    else if (_gameBoard[x, y] == GameCell.HS)
                    {
                        // if game isn't over keep cell hidden
                        Console.Write((_gameOver) ? "s\t" : "-\t");
                    }
                    else if (_gameBoard[x, y] == GameCell.RE)
                    {
                        Console.Write("M\t");
                    }
                    else if (_gameBoard[x, y] == GameCell.RS)
                    {
                        Console.Write("X\t");
                    }
                }

                Console.WriteLine();
            }
        }


        /// <summary>
        /// Allow the user to set up the game board by choosing where to place their ships
        /// Checks board boundaries and for overlapping ships
        /// </summary>
        private void SetBoard()
        {
            _gameOver = true;
            int[] shipSizes = { 2, 3, 4 };

            // loop through all the different ship sizes
            foreach (int i in shipSizes)
            {
                bool shipPlaced = false; // ships start out unplaced

                // place the ship looping until the current ship is placed
                while (!shipPlaced)
                {
                    // display the current board setup so the user can see where the other ships are
                    Console.WriteLine("\nCurrent Board Setup:");
                    PrintBoard();

                    // get the ship's starting row and column position
                    Console.WriteLine("Where would you like to place the ship with length " + i);
                    Console.Write("row: ");
                    int row = int.Parse(Console.ReadLine());
                    Console.Write("col: ");
                    int col = int.Parse(Console.ReadLine());

                    // get the direction of the ship to be placed
                    Console.Write("Which direction would you like it to face (r or d): ");
                    char dir = Console.ReadLine().ToLower()[0];

                    // check which direction the ship is to be placed
                    if ('r' == dir)
                    {
                        int end = col + i - 1; // get the last col of the ship
                        if (6 >= end)
                        {
                            // store the row without using the actual board
                            GameCell[] tempRow = new GameCell[_gameBoard.GetLength(0)];

                            // copy the board's current row into a temp array
                            for (int x = 0; x < _gameBoard.GetLength(0); x++)
                            {
                                tempRow[x] = _gameBoard[row, x];
                            }

                            // check if there are any ships in the current row
                            int check = 0;
                            for (int x = col; x <= end; x++)
                            {
                                // if the cell is empty
                                if (GameCell.HE.Equals(tempRow[x]))
                                {
                                    tempRow[x] = GameCell.HS; // place the ship
                                }
                                else
                                {
                                    // hit another ship!!
                                    Console.WriteLine("Sorry cannot place ship here!");
                                    check = -1; // cannot place the ship at this location
                                    break;
                                }
                            }

                            // if there weren't any ships copy the row into the game board array
                            if (check != -1)
                            {
                                for (int x = 0; x < _gameBoard.GetLength(0); x++)
                                {
                                    _gameBoard[row, x] = tempRow[x];
                                }
                                shipPlaced = true; // mark the ship as placed
                            }
                        }
                        else
                        {
                            Console.WriteLine("Sorry ship cannot go off the board!"); // the ship cannot go off
                        }
                    }
                    else if ('d' == dir)
                    {
                        int end = row + i - 1; // get the last row of the ship
                        if (6 >= end)
                        {
                            // store the column without using the actual board
                            GameCell[] tempCol = new GameCell[_gameBoard.GetLength(1)];

                            // copy the game board's current column into a temp array
                            for (int x = 0; x < _gameBoard.GetLength(1); x++)
                            {
                                tempCol[x] = _gameBoard[x, col];
                            }

                            // check if there are any ships in the current column
                            int check = 0;
                            for (int x = row; x <= end; x++)
                            {
                                // if the cell is empty
                                if (GameCell.HE.Equals(tempCol[x]))
                                {
                                    tempCol[x] = GameCell.HS; // place the ship
                                }
                                else
                                {
                                    // hit another ship!!
                                    Console.WriteLine("Sorry cannot place ship here!");
                                    check = -1; // cannot place ship at this location
                                    break;
                                }
                            }

                            // if there weren't any ships copy the column into the game board array
                            if (check != -1)
                            {
                                for (int x = 0; x < _gameBoard.GetLength(1); x++)
                                {
                                    _gameBoard[x, col] = tempCol[x];
                                }
                                shipPlaced = true; // mark the ship as placed
                            }
                        }
                        else
                        {
                            Console.WriteLine("Sorry ship cannot go off the board!"); // the ship cannot go off
                        }
                    }
                    else
                    {
                        Console.WriteLine("Sorry direction \'" + dir + "\' not recognized!"); // not recognized
                    }

                }
            }
            // display the final set up board
            Console.WriteLine("\nFinal Board Setup:");
            PrintBoard();

            _gameOver = false;
        }

    }
}
