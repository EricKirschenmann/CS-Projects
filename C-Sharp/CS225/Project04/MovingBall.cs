﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project04
{
    internal class MovingBall : Ball, IBallMethods
    {
        // boolean that stores if the ball is falling, falling by default
        private bool isFalling = true;

        /**
         * Constructor that calls the super constructor
         *
         * @param radius the ball's radius
         * @param bounceSpeedLoss the ball's bounce speed loss
         * @param ballImageName the ball's image file name
         */
        internal MovingBall(double radius, double bounceSpeedLoss, String ballImageName) : base(radius, bounceSpeedLoss, ballImageName)
        {
        }

        /**
         * Throw the ball. This method does not really move the ball, but just SETS
         * it off on its throw. Thus, it merely sets the following internal params:
         * - Ball's horizontal velocity (xVelocity) to the hVelocity passed
         * in, which is in pixels-per-frame
         * - Ball's vertical velocity (yVelocity) to 0 pixels-per-frame
         * - Ball's height of it's center (ballCenterY) to the startingHeight,
         * which is in pixels
         * - Ball's x position (in pixels) of it's center (ballCenterX) to a
         * point that places the ball just off the screen
         *
         * @param hVelocity The horizontal velocity
         * @param startingHeight The ball's starting height
         */
        public void ThrowBall(double hVelocity, double startingHeight)
        {
            xVelocity = hVelocity; // set initial horizontal velocity
            yVelocity = 0; // set initial vertical velocity
            ballCenterY = startingHeight; // set the starting height
            ballCenterX = radius * -1; // set the starting horizontal location
            ballStillBouncing = true; // set the ball to bouncing
        }

        /**
         * Roll the ball. This method does not really move the ball, but just SETS
         * it off on its roll. Thus, it merely sets the following internal params:
         * - Ball's horizontal velocity (xVelocity) to the hVelocity passed
         * in, which is in pixels-per-frame
         * - Ball's height of it's center (ballCenterY) to point so that the
         * ball appears to be sitting on the ground
         * - Ball's x position (in pixels) of it's center (ballCenterX) to a
         * point that places the ball just off the screen
         *
         * @param hVelocity The ball's horizontal velocity
         */
        public void RollBall(double hVelocity)
        {
            xVelocity = hVelocity; // set the initial horizontal velocity
            ballCenterY = radius; // set the initial vertical location
            ballCenterX = radius * -1; // set the starting horizontal location
        }

        /**
         * Move the ball to the right one frame. This is essentially moving the ball
         * a number of pixels which is equal to the ball's xVelocity (which was set
         * earlier by the RollBall method).
         *
         * The ball must also rotate at the CORRECT rate (e.g., if the circumference of
         * a ball is 300 pixels (can compute from the ball's radius), then the ball
         * must make one full rotation after moving 300 pixels.
         */
        public void AdvanceRollingBallSingleFrame()
        {
            ballCenterX += xVelocity; // move the ball forward at the x velocity
            degreesRotated += radius * 2.0 * Math.PI * -xVelocity; // rotate the ball
        }

        /**
         * Move the ball to the right one frame. This is essentially moving the ball a number of pixels
         * which is equal to the ball's xVelocity (which was set earlier by the ThrowBall method.
         *
         * The ball must also FALL with an acceleration of 9.8pixels/(frame*frame). That is, each time the
         * ball is advanced, it's vertical velocity is also updated using the following velocity equation
         * derived from physics: newVelocity = initialVelocity + 9.8        (we'll assume time always = 1
         * frame)
         *
         * When the ball hits the ground, it immediately loses some percent of it's speed, according to
         * the bounceSpeedLoss (see below) variable in the Ball class, and then changes it's direction to
         * start moving up. The ball will then begin to decelerate using the following equation:
         * newVelocity = initialVelocity - 9.8            (we'll assume time always = 1 frame)
         *
         * NOTE: bounceSpeedLoss will be a number between 0-1. It should be applied as follows: vVelocity
         * = 1 - (vVelocity*bounceSpeedLoss); Thus, if your velocity was 10, and bounceSpeedLoss was .15,
         * the new velocity will be 8.5.
         *
         * Once the ball's velocity reaches 0 on it's upward trajectory, it simply changes direction to
         * start falling again.
         *
         * After a few bounces, the ball eventually bounces lower and lower. Each time the ball bounces
         * and hits the ground, this method should examine the yVelocity at which the ball will be
         * bouncing upwards. If the velocity is less than 10, then the ball should set the internal
         * variable ballStillBouncing to false.
         */
        public void AdvanceThrownBallSingleFrame()
        {
            ballCenterX += xVelocity; // move the ball forward

            // check if the ball is falling or not
            if (isFalling)
            {
                yVelocity += 9.8; // if falling it is accelerating at gravity
                ballCenterY -= yVelocity; // move the ball down
            }
            else if (!isFalling)
            {
                yVelocity -= 9.8; // if not falling slowing down by gravity
                ballCenterY += yVelocity; // move the ball up
            }

            // check if the ball is at the top of the bounce
            if (yVelocity <= 0)
            {
                isFalling = true; // the ball is now falling
            }
            else if (ballCenterY <= radius)
            { // if the ball hits the ground
                ballCenterY = radius; // the ball is at its lowest point
                yVelocity = (yVelocity * (1 - bounceSpeedLoss)); // slow down every bounce
                isFalling = false; // the ball is now going up

                // if the ball has slowed down enough it stops bouncing
                if (yVelocity < 10)
                {
                    ballStillBouncing = false;
                }
            }

            // rotate the ball
            degreesRotated += radius * 2.0 * Math.PI * -xVelocity;
        }

        /**
         * Simply returns ballStillBouncing
         *
         * @return {@code true} if the ball is still bouncing, {@code false} otherwise
         */
        public bool IsThrownBallStillBouncing()
        {
            return ballStillBouncing;
        }
    }
}
