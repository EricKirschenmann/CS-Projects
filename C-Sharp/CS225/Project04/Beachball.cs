﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project04
{
    class Beachball : MovingBall
    {
        internal Beachball() : base(150, 0.6, "beachball.png")
        {

        }
    }
}
