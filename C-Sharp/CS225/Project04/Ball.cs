﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project04
{
    internal abstract class Ball
    {
        internal double ballCenterX;       // The ball's x position (in pixels) on the court
        internal double ballCenterY;       // The ball's y position (in pixels) on the court
        internal double radius;         // The ball's radius
        internal double xVelocity;       // The ball's horizontal velocity, in pixels-per-frame
        internal double yVelocity;       // The ball's horizontal velocity, in pixels-per-frame
        internal double degreesRotated;     // The ball's rotation, in degrees
        internal double bounceSpeedLoss;     // The percentage of speed (0-1) which the ball instantly loses when hitting ground
        internal bool ballStillBouncing; // Tells whether ball is still bouncing
        internal String ballImageName;       // String which holds image name representing the ball

        /**
         * Overloaded Constructor
         *
         * @param radius The Ball's Radius
         * @param bounceSpeedLoss The Ball's Bounce Speed Loss
         * @param ballImageName The name of the ball image file
         */
        internal Ball(double radius, double bounceSpeedLoss, String ballImageName)
        {
            ballCenterX = 0;
            ballCenterY = 0;
            degreesRotated = 0;
            ballStillBouncing = false;

            this.radius = radius;
            this.bounceSpeedLoss = bounceSpeedLoss;
            this.ballImageName = ballImageName;
        }

        // Getters (for the Court class to use)
        internal String GetBallImageName()
        {
            return ballImageName;
        }

        internal double GetRadius()
        {
            return radius;
        }

        internal double GetBallCenterX()
        {
            return ballCenterX;
        }

        internal double GetBallCenterY()
        {
            return ballCenterY;
        }

        internal double GetDegreesRotated()
        {
            return degreesRotated;
        }

        internal double GetBounceSpeedLoss()
        {
            return bounceSpeedLoss;
        }
    }
}
