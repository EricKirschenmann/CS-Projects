﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project04
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        /**
         * Rolls the Ball
         *
         * @param b The MovingBall Object
         */
        private static void RollBall(MovingBall b)
        {
            int name = 0;
            // get the ball type to append to the filename
            String type = b.GetBallImageName();
            type = type.Substring(0, type.LastIndexOf(".png"));
            type += " - Roll ";

            b.RollBall(20);
            while ((b.GetBallCenterX() < Court.getCourtLength()))
            {
                b.AdvanceRollingBallSingleFrame();
                Console.WriteLine("Frame " + name + " output.");
                Court.DrawBallOnCourt(b, type + (name++).ToString());

            }
        }
    }
}
