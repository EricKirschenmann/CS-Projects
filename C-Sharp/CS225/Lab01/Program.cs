﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab01
{
    class Program
    {
        static void Main(string[] args)
        {
            // get a string then pass that through the methods
            Console.Write("Enter a string: ");
            PrintNumVowels(StringToCharArray(Console.ReadLine()));
        }

        static char[] StringToCharArray(string s)
        {
            char[] chars = new char[s.Length];

            // copy each character from the string into the array
            for (int x = 0; x < chars.Length; x++)
            {
                chars[x] = s[x];
            }

            return chars;
        }

        static void PrintNumVowels(char[] chars)
        {
            int[] vowels = new int[5];

            // go through each character in the array
            foreach (char c in chars)
            {
                // check if the current character is a vowel if it is count it
                switch (c)
                {
                    case 'A':
                    case 'a':
                        vowels[0]++;
                        break;
                    case 'E':
                    case 'e':
                        vowels[1]++;
                        break;
                    case 'I':
                    case 'i':
                        vowels[2]++;
                        break;
                    case 'O':
                    case 'o':
                        vowels[3]++;
                        break;
                    case 'U':
                    case 'u':
                        vowels[4]++;
                        break;
                    default:
                        break;
                }
            }

            // display the count of each vowel
            Console.WriteLine($"A:{vowels[0]} E:{vowels[1]} I:{vowels[2]} O:{vowels[3]} U:{vowels[4]}");
        }
    }
}
