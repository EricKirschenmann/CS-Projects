﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    internal class Employee : UniqueObject
    {
        // instance variables
        private string _firstName;
        private string _lastName;
        protected internal double _hourlyRate;
        protected internal double _years;

        /// <summary>
        /// Get the first name
        /// </summary>
        protected string FirstName => _firstName;

        /// <summary>
        /// Get the last name
        /// </summary>
        protected string LastName => _lastName;

        /// <summary>
        /// Get the hourly rate
        /// </summary>
        protected double HourlyRate => _hourlyRate;

        /// <summary>
        /// Get the number of years
        /// </summary>
        protected double Years => _years;

        /// <summary>
        /// Calculate and return the yearly salary
        /// </summary>
        protected double YearlySalary => HourlyRate * 40 * 52;

        /// <summary>
        /// Default Constructor
        /// </summary>
        internal Employee() : base()
        {
            _firstName = "John";
            _lastName = "Doe";
            _hourlyRate = 9.00;
            _years = 0.0;
        }

        /// <summary>
        /// Overloaded Constructor
        /// </summary>
        /// <param name="firstName">The first name of the employee</param>
        /// <param name="lastName">The last name of the employee</param>
        /// <param name="hourlyRate">The hourly rate of the employee</param>
        /// <param name="years">The number of years the employee has been with the company</param>
        public Employee(string firstName, string lastName, double hourlyRate, double years) : base()
        {
            _firstName = firstName;
            _lastName = lastName;
            _hourlyRate = hourlyRate;
            _years = years;
        }

        /// <summary>
        /// Promotes this employee by adding to their hourly rate and years
        /// </summary>
        public virtual void Promote()
        {
            _hourlyRate += _hourlyRate * 0.03;
            _years++;
        }

        /// <summary>
        /// Create and return a readable string about this object
        /// </summary>
        /// <returns>Readable string containing information about this object</returns>
        public override string ToString()
        {
            return $"{FirstName} {LastName} (#{UniqueIDString()}) has been with the company for {Years} years and makes {HourlyRate:C2}/hr.";
        }
    }
}
