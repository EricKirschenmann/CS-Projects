﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    class Manager : Employee
    {
        // contains this manager's employees
        private List<Employee> _managedEmployees;

        /// <summary>
        /// Default Constructor
        /// </summary>
        internal Manager()
        {
            _managedEmployees = new List<Employee>();
        }

        /// <summary>
        /// Overloaded Constructor
        /// </summary>
        /// <param name="firstName">The first name of the employee</param>
        /// <param name="lastName">The last name of the employee</param>
        /// <param name="hourlyRate">The hourly rate of the employee</param>
        /// <param name="years">The number of years the employee has been with the company</param>
        internal Manager(string firstName, string lastName, double hourlyRate, double years) : base(firstName, lastName, hourlyRate, years)
        {
            _managedEmployees = new List<Employee>();
        }

        /// <summary>
        /// Adds the employee object to the List
        /// </summary>
        /// <param name="employee">Employee object to add to List</param>
        public void AddEmployee(Employee employee)
        {
            _managedEmployees.Add(employee);
        }

        /// <summary>
        /// Remove the employee with the _uniqueID provided
        /// </summary>
        /// <param name="id">The _uniqueID of the employee to remove</param>
        public void RemoveEmployee(long id)
        {
            foreach (Employee employee in _managedEmployees)
            {
                // check current employee for matching id
                if (employee.UniqueID == id)
                {
                    // remove employee
                    _managedEmployees.Remove(employee);
                    break;
                }
            }
        }

        /// <summary>
        /// Promote this Manager object, overriden with better rate increase
        /// </summary>
        public override void Promote()
        {
            _hourlyRate += (_hourlyRate * .05) + (_managedEmployees.Count * 0.25);
            _years++;
        }
    }
}
