﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    internal class UniqueObject
    {
        // holds the value of the next unique id to assign
        private static long _nextUniqueID = 0;

        // holds the unique id for an instance of the class
        private long _uniqueID;

        internal UniqueObject()
        {
            // set the unique id to the next available, then increment the next unique id
            _uniqueID = _nextUniqueID++;
        }

        /// <summary>
        /// Return the value stored within _uniqueID
        /// </summary>
        protected internal long UniqueID { get => _uniqueID; }

        /// <summary>
        /// Formats and returns the _uniqueID variable as a string with zeros padding
        /// </summary>
        /// <returns>A string with leading zeros representing the UniqueID</returns>
        protected internal string UniqueIDString()
        {
            return _uniqueID.ToString("D4");
        }
    }
}
