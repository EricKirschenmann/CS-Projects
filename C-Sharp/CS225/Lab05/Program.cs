﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> externalEmployeeList = new List<Employee>();

            Manager manager = new Manager("Jer", "Matson", 20.0, 5);
            externalEmployeeList.Add(manager);

            string[] firstNames = { "Bill", "Sue", "Jim", "Jon", "Katy", "Eddie", "Martha" };
            string[] lastNames = { "Smith", "Jones", "Brown", "Black", "Ortiz", "Ricks", "Kim" };

            for (int i = 0; i < 7; i++)
            {
                Employee e = new Employee(firstNames[i], lastNames[i], (10.0 + i), (i + 1));
                manager.AddEmployee(e);
                externalEmployeeList.Add(e);
            }

            manager.RemoveEmployee(2);

            externalEmployeeList[0].Promote();
            externalEmployeeList[2].Promote();
            externalEmployeeList[4].Promote();
            externalEmployeeList[5].Promote();

            foreach (Employee employee in externalEmployeeList)
            {
                Console.WriteLine(employee.ToString());
            }
        }
    }
}
